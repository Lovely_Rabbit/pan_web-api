﻿using System.Collections.Generic;

namespace Pan.Common.Helper.Email
{
    public class EmailModel
    {

    }

    /// <summary>
    /// 邮件消息和内容
    /// </summary>
    public class EmailMsg
    {
        public string Email { get; set; }
        public string Messg { get; set; }
    }

    public class EmailSendEail
    {
        /// <summary>
        /// 发邮件的账号
        /// </summary>
        public string Accoutnt { get; set; }

        /// <summary>
        /// 生成的授权码 非 账号密码
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// 发送邮件的来自方
        /// </summary>
        public string FromDisplay { get; set; }

        /// <summary>
        /// 收件人
        /// </summary>
        public IList<string> tos { get; set; }

        /// <summary>
        /// 抄送人
        /// </summary>
        public IList<string> ccs { get; set; }

        /// <summary>
        /// 密送人
        /// </summary>
        public IList<string> bcc { get; set; }

        /// <summary>
        /// 内容
        /// </summary>
        public string content { get; set; }

        /// <summary>
        /// 主题
        /// </summary>
        public string title { get; set; }
    }
}
