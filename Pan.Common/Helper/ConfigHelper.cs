﻿using Microsoft.Extensions.Configuration;
using System;

namespace Pan.Common.Helper
{
    public class ConfigHelper
    {
        private static IConfiguration _configuration;

        private ConfigHelper()
        {
        }

        public static void Init(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //读取config单个值
        public static string GetSectionValue(string key)
        {
            string str = _configuration.GetSection(key).Value;
            return string.IsNullOrEmpty(str) ? "" : str;
        }

        private static IConfigurationSection GetConfigurationSection(string key)
        {
            //_configuration.GetSection("SqlServer").Bind(sqlServerConn);
            return _configuration.GetSection(key);
        }

        /// <summary>
        /// 根据Key读取并赋值给对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static T Bind<T>(string key, T obj)
        {
            GetConfigurationSection(key).Bind(obj);
            return obj;
        }

        /// <summary>
        /// 绑定静态对象
        /// </summary>
        /// <param name="type">类型</param>
        /// <param name="flag">如果是子对象绑定，需传false，如EmailConfig一样</param>
        public static void BindStatic(Type type, bool flag = true, string name = "")
        {
            var listProp = type.GetProperties();
            foreach (var item in listProp)
            {
                try
                {
                    string key = flag ? item.Name : ($"{(string.IsNullOrEmpty(name) ? type.Name : name)}:{item.Name}");
                    var ctor = item.PropertyType.GetConstructor(new Type[] { });
                    if (ctor != null)//判断类型是否有无参构造函数，如果没有就创建实例进行绑定
                    {
                        var obj = Activator.CreateInstance(item.PropertyType);
                        Bind(key, obj);
                        item.SetValue(null, obj);
                    }
                    else//否则就直接赋值
                    {
                        var obj = Convert.ChangeType(GetSectionValue(key), item.PropertyType);
                        item.SetValue(null, obj);
                    }
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }
    }
}