﻿using System.Collections.Generic;

namespace Pan.Common.Helper
{
    public static class DictionaryHelper
    {
        public static string TageNameDic(int tag)
        {
            Dictionary<int, string> dic = new Dictionary<int, string>();
            dic.Add(1, "影视");
            dic.Add(2, "图片");
            dic.Add(3, "软件");
            dic.Add(4, "游戏");
            dic.Add(5, "音乐");
            dic.Add(6, "动漫");
            dic.Add(7, "书籍");
            dic.Add(9, "学习");
            dic.Add(10, "求助");
            string name = dic.ContainsKey(tag) ? dic[tag] : "";
            return name;
        }
    }
}