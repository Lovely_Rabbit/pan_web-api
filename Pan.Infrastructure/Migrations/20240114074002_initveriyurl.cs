﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;

#nullable disable

namespace Pan.Infrastructure.Migrations
{
    public partial class initveriyurl : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.DropColumn(
            //    name: "post_id",
            //    table: "post_thread");

            //migrationBuilder.DropColumn(
            //    name: "post_id",
            //    table: "post_html");

            //migrationBuilder.AddColumn<string>(
            //    name: "AliyunpanUrl",
            //    table: "post_thread",
            //    type: "longtext",
            //    nullable: true)
            //    .Annotation("MySql:CharSet", "utf8mb4");

            //migrationBuilder.AddColumn<bool>(
            //    name: "IsUpdate",
            //    table: "post_thread",
            //    type: "tinyint(1)",
            //    nullable: false,
            //    defaultValue: false);

            //migrationBuilder.AddColumn<string>(
            //    name: "TagName",
            //    table: "post_thread",
            //    type: "longtext",
            //    nullable: true)
            //    .Annotation("MySql:CharSet", "utf8mb4");

            //migrationBuilder.AddColumn<string>(
            //    name: "hash_id",
            //    table: "post_thread",
            //    type: "longtext",
            //    nullable: true)
            //    .Annotation("MySql:CharSet", "utf8mb4");

            //migrationBuilder.AddColumn<string>(
            //    name: "hash_id",
            //    table: "post_html",
            //    type: "longtext",
            //    nullable: true)
            //    .Annotation("MySql:CharSet", "utf8mb4");

            //migrationBuilder.AddColumn<int>(
            //    name: "IsUpdate",
            //    table: "post",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            //migrationBuilder.AddColumn<string>(
            //    name: "hash_id",
            //    table: "post",
            //    type: "longtext",
            //    nullable: true)
            //    .Annotation("MySql:CharSet", "utf8mb4");

            //migrationBuilder.AddColumn<int>(
            //    name: "parent_id",
            //    table: "n_hotNewType",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            migrationBuilder.CreateTable(
                name: "verifyUrl",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ShareId = table.Column<string>(type: "varchar(125)", maxLength: 125, nullable: true)
                        .Annotation("MySql:CharSet", "utf8mb4"),
                    PostId = table.Column<int>(type: "int", nullable: false),
                    Status = table.Column<int>(type: "int", nullable: false),
                    CreateOn = table.Column<DateTime>(type: "datetime(6)", nullable: true),
                    UpdateOn = table.Column<DateTime>(type: "datetime(6)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_verifyUrl", x => x.Id);
                })
                .Annotation("MySql:CharSet", "utf8mb4");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "verifyUrl");

            //migrationBuilder.DropColumn(
            //    name: "AliyunpanUrl",
            //    table: "post_thread");

            //migrationBuilder.DropColumn(
            //    name: "IsUpdate",
            //    table: "post_thread");

            //migrationBuilder.DropColumn(
            //    name: "TagName",
            //    table: "post_thread");

            //migrationBuilder.DropColumn(
            //    name: "hash_id",
            //    table: "post_thread");

            //migrationBuilder.DropColumn(
            //    name: "hash_id",
            //    table: "post_html");

            //migrationBuilder.DropColumn(
            //    name: "IsUpdate",
            //    table: "post");

            //migrationBuilder.DropColumn(
            //    name: "hash_id",
            //    table: "post");

            //migrationBuilder.DropColumn(
            //    name: "parent_id",
            //    table: "n_hotNewType");

            //migrationBuilder.AddColumn<int>(
            //    name: "post_id",
            //    table: "post_thread",
            //    type: "int",
            //    nullable: false,
            //    defaultValue: 0);

            //    migrationBuilder.AddColumn<int>(
            //        name: "post_id",
            //        table: "post_html",
            //        type: "int",
            //        nullable: false,
            //        defaultValue: 0);
        }
    }
}
