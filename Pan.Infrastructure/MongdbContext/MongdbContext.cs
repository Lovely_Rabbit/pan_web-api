﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static System.Collections.Specialized.BitVector32;

namespace Pan.Infrastructure.MongdbContext
{
    public class MongoDBContext : IMongoContext
    {
        //此构造方法主要目的是接连接字符串的配置和要操作哪个数据库
        //安装包MongoDB.Driver
        //private MongoClient _mongoClent;//mongodb的连接
        private readonly MongoDBContextOptions _options;  //使用强类型来接受配置信息
        private IMongoDatabase _database;
        private MongoClient _mongoClient;
        private readonly List<Func<Task>> _commands;
        public IClientSessionHandle? Session = null;


        /// <summary>
        /// 构造函数中接收传过来的配置信息(连接字符串+操作的对应数据库)options
        /// </summary>
        /// <param name="optionsAccessor"></param>
        public MongoDBContext(IOptions<MongoDBContextOptions> optionsAccessor)
        {
            _options = optionsAccessor.Value;
            _commands = new List<Func<Task>>();
            _mongoClient = new MongoClient(_options.MongoDBConnection);  //通过options中存放的连接字符串, 起连接
            _database = _mongoClient.GetDatabase(_options.dbName); //通过options中存放的数据库名称,获取数据库

        }

        /// <summary>
        /// 获取mongodb 的连接
        /// </summary>
        public MongoClient MongoClent { get { return _mongoClient; } }

        /// <summary>
        /// 获取连接的数据库
        /// </summary>
        public IMongoDatabase Database { get { return _database; } }


        /// <summary>
        /// 添加命令操作
        /// </summary>
        /// <param name="func">委托</param>
        /// <returns></returns>
        public async Task AddCommandAsync(Func<Task> func)
        {
            _commands.Add(func);
            await Task.CompletedTask;
        }


        /// <summary>
        /// 保存更改
        /// TODO：MongoDB单机服务器不支持事务【使用MongoDB事务会报错：Standalone servers do not support transactions】,只有在集群情况下才支持事务
        /// 原因：MongoDB在使用分布式事务时需要进行多节点之间的协调和通信，而单机环境下无法实现这样的分布式协调和通信机制。但是，在MongoDB部署为一个集群（cluster）后，将多个计算机连接为一个整体，通过协调和通信机制实现了分布式事务的正常使用。从数据一致性和可靠性的角度来看，在分布式系统中实现事务处理是至关重要的。而在单机环境下不支持事务，只有在集群情况下才支持事务的设计方式是为了保证数据一致性和可靠性，并且也符合分布式系统的设计思想。
        /// </summary>
        /// <returns></returns>
        public async Task<int> SaveChangesAsync()
        {
            using (Session = await _mongoClient.StartSessionAsync())
            {
                Session.StartTransaction();


                var commandTasks = _commands.Select(c => c());
                await Task.WhenAll(commandTasks);


                await Session.CommitTransactionAsync();
            }
            return _commands.Count;
        }




        /// <summary>
        /// 获取MongoDB集合
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="name">集合名称</param>
        /// <returns></returns>
        public IMongoCollection<T> GetCollection<T>(string name)
        {
            return _database.GetCollection<T>(name);
        }


        /// <summary>
        /// 释放上下文
        /// </summary>
        public void Dispose()
        {
            Session?.Dispose();
            GC.SuppressFinalize(this);
        }

    }


    //public class MongoDBContext
    //{
    //    private readonly IMongoDatabase _database;

    //    public MongoDBContext(IOptions<MongoDBContextOptions> settings)
    //    {
    //        var client = new MongoClient(settings.Value.ConnectionString);
    //        _database = client.GetDatabase(settings.Value.DatabaseName);
    //    }

    //    public IMongoCollection<T> GetCollection<T>(string name)
    //    {
    //        return _database.GetCollection<T>(name);
    //    }
    //}



    public class MongoDBContextOptions
    {
        public string MongoDBConnection { get; set; }//连接MongoDB字符串
        public string dbName { get; set; } //连接对应的数据库名称--MongoDB需要指定连接的数据库
                                           // public MongoDBContextOptions Value { get { return this; } }
    }
}
