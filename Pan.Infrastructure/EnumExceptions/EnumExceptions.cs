﻿using System;
using System.ComponentModel;

namespace Pan.Infrastructure.EnumExceptions
{
    public static class EnumExceptions
    {
        // 获取方法
        public static string GetDescriptionByEnum(this Enum enumValue)
        {
            // 调用示例 GetDescriptionByEnum(enumStudent.age) → 年龄
            string value = enumValue.ToString();
            System.Reflection.FieldInfo field = enumValue.GetType().GetField(value);
            object[] objs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);    //获取描述属性
            if (objs.Length == 0)    //当描述属性没有时，直接返回名称
                return value;
            DescriptionAttribute descriptionAttribute = (DescriptionAttribute)objs[0];
            return descriptionAttribute.Description;
        }

        /// <summary>
        /// 根据 Descript 获取 特性
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="description"></param>
        /// <returns></returns>
        /// <exception cref="ArgumentException"></exception>
        public static T GetEnumByDescription<T>(string description) where T : Enum
        {
            // 调用示例
            // GetEnumByDescription<enumStudent>("性别").ToString() → sex
            System.Reflection.FieldInfo[] fields = typeof(T).GetFields();
            foreach (System.Reflection.FieldInfo field in fields)
            {
                object[] objs = field.GetCustomAttributes(typeof(DescriptionAttribute), false);    //获取描述属性
                if (objs.Length > 0 && (objs[0] as DescriptionAttribute).Description == description)
                {
                    return (T)field.GetValue(null);
                }
            }

            throw new ArgumentException(string.Format("{0} 未能找到对应的枚举.", description), "Description");
        }

    }
}
