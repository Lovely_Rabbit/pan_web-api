﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pan.Infrastructure.Elasticsearch
{
    /// <summary>
    /// Elasticsearch 节点
    /// </summary>
    public class ElasticsearchNode
    {
        /// <summary>
        /// 主机
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// 端口号
        /// </summary>
        public string Port { get; set; }

        /// <summary>
        /// 输出字符串
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            var port = Port == "" ? "" : $":{Port}";

            var result = $"{Host}{port}".ToLowerInvariant();
            return result.IndexOf("http", StringComparison.OrdinalIgnoreCase) > -1 ? result : $"http://{result}";
        }
    }
}
