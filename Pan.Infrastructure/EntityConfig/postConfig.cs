﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pan.Infrastructure.Entity;

namespace Pan.Infrastructure.EntityConfig
{
    public class postConfig : IEntityTypeConfiguration<post>
    {
        public void Configure(EntityTypeBuilder<post> builder)
        {
            builder.ToTable("post");
            builder.Property(x => x.del_url).HasMaxLength(256);
        }
    }
}
