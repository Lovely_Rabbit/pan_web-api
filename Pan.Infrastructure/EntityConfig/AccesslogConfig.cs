﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pan.Infrastructure.Entity;

namespace Pan.Infrastructure.EntityConfig
{
    public class AccesslogConfig : IEntityTypeConfiguration<AccessLog>
    {
        public void Configure(EntityTypeBuilder<AccessLog> builder)
        {
            builder.ToTable("accesslog");
            builder.Property(x => x.request_url).HasMaxLength(255);
            builder.Property(x => x.request_ip).HasMaxLength(125);
            builder.Property(x => x.Referer).HasMaxLength(225);
            builder.Property(x => x.request_ForwardIp).HasMaxLength(225);
            builder.Property(x => x.Params).HasMaxLength(225);
        }
    }
}
