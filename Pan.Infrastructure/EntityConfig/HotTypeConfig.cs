﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Pan.Infrastructure.Entity;

namespace Pan.Infrastructure.EntityConfig
{
    public class HotTypeConfig : IEntityTypeConfiguration<HotNewType>
    {
        public void Configure(EntityTypeBuilder<HotNewType> builder)
        {
            builder.ToTable("n_hotNewType");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.HotName).HasMaxLength(125);
            builder.Property(x => x.HotHref).HasMaxLength(525);
        }
    }
}
