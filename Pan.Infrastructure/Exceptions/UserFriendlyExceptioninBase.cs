﻿using System;

namespace Pan.Infrastructure.Exceptions
{
    public class UserFriendlyExceptioninBase : Exception
    {
        public UserFriendlyExceptioninBase()
        {
        }

        public UserFriendlyExceptioninBase(string errorMessage)
        {
            ErrorMessage = errorMessage;
        }

        public string ErrorMessage { get; set; }

        public virtual int Code { get; set; } = 200;
    }
}
