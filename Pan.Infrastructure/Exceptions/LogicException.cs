﻿namespace Pan.Infrastructure.Exceptions
{
    public class LogicException : UserFriendlyExceptioninBase
    {
        public LogicException(string errorMessage) : base(errorMessage)
        {
        }

        public override int Code { get; set; } = 502;
    }
}
