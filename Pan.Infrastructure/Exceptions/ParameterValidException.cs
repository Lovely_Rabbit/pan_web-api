﻿namespace Pan.Infrastructure.Exceptions
{
    public class ParameterValidException : UserFriendlyExceptioninBase
    {
        public ParameterValidException(string errorMessage) : base(errorMessage)
        {
        }

        public override int Code { get; set; } = 503;
    }
}
