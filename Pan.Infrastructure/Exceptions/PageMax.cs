﻿namespace Pan.Infrastructure.Exceptions
{
    public class MaxResultPageMaxException : UserFriendlyExceptioninBase
    {
        public MaxResultPageMaxException()
        {
            ErrorMessage = "请传递正确的MaxResultCount值 ！";
        }

        public MaxResultPageMaxException(string errorMessage) : base(errorMessage) { }

        public override int Code { get; set; } = 400;
    }

    public class MaxResultPageSkipException : UserFriendlyExceptioninBase
    {
        public MaxResultPageSkipException()
        {
            ErrorMessage = "请传递正确的SkipCount值 ！";
        }

        public MaxResultPageSkipException(string errorMessage) : base(errorMessage) { }

        public override int Code { get; set; } = 400;
    }

    public class MQUpdateExceptionin : UserFriendlyExceptioninBase
    {
        public MQUpdateExceptionin()
        {
            ErrorMessage = "MQ获取数据失败！";
        }

        public MQUpdateExceptionin(string errorMessage) : base(errorMessage) { }

        public override int Code { get; set; } = 400;
    }


}
