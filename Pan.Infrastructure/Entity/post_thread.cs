﻿using Pan.Infrastructure.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pan.Infrastructure.Entity
{
    public partial class post_thread : BaseEntity
    {
        public string thread { get; set; }
        // public int post_id { get; set; }
        public string AliyunpanUrl { get; set; }
        public bool IsUpdate { get; set; }
        public string TagName { get; set; }

        /// <summary>
        /// 数据唯一id
        /// </summary>
        [Column("hash_id")]
        public string HashId { get; set; }

        [Column("hash_v")]
        public string HashV { get; set; }
    }

    public class Post_Thread
    {
        /*

       {
        "Id": 0,
"HashId": "YWx5dW5wYW4uY29tXzc0ODY1",
"Title": "油脂 Grease Live! (2016)",
"AliUrl": "",
"RequestId": "YWx5dW5wYW4uY29tXzc0ODY1"
}
*/
        public int Id { get; set; }

        public string HashId { get; set; }

        public string Title { get; set; }

        public string AliUrl { get; set; }

        public string RequestId { get; set; }

        public string TagsTitle { get; set; }

        public int Tags { get; set; }
    }
}
