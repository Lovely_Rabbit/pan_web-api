﻿using Pan.Infrastructure.Base;

namespace Pan.Infrastructure.Entity
{
    public class HotNew : BaseEntity
    {
        /// <summary>
        /// 热榜链接
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// 主外键
        /// </summary>
        public int HotType { get; set; }

        /// <summary>
        /// 文章链接
        /// </summary>
        public string HotNewHref { get; set; }

    }
}
