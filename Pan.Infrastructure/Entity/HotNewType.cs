﻿using Pan.Infrastructure.Base;

namespace Pan.Infrastructure.Entity
{
    public class HotNewType : BaseEntity
    {
        /// <summary>
        /// 热榜名称
        /// </summary>
        public string HotName { get; set; }

        /// <summary>
        /// 热榜链接
        /// </summary>
        public string HotHref { get; set; }

        public int parent_id { get; set; }

    }
}
