﻿using Pan.Infrastructure.Base;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pan.Infrastructure.Entity
{
    public class post_html : BaseEntity
    {
        //public int Id { get; set; }
        public string html_context { get; set; }
        // public int post_id { get; set; }

        [Column("hash_id")]
        public string hash_id { get; set; }

        [Column("hash_v")]
        public string HashV { get; set; }

        //   public string Status { get; set; }
    }
}
