﻿using Pan.Infrastructure.Base;
using System;

#nullable disable

namespace Pan.Infrastructure.Entity
{
    public class AccessLog : BaseEntity
    {
        public string request_ip { get; set; }
        public string request_ForwardIp { get; set; }
        public string request_url { get; set; }
        public DateTime request_date { get; set; }
        public string Referer { get; set; }
        public string User_Agent { get; set; }

        /// <summary>
        /// 请求的参数
        /// </summary>
        public string Params { get; set; }

        /// <summary>
        /// 请求次数
        /// </summary>
        public int request_count { get; set; } = 1;
    }
}
