﻿using System;

namespace Pan.Infrastructure.Base
{
    public interface IHasCreationTime
    {
        public DateTime? CreateOn { get; set; }
    }

    public interface IHasCreation : IHasCreationTime
    {
        public string CreatorUser { get; set; }
    }
}
