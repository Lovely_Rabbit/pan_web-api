﻿using System;

namespace Pan.Infrastructure.Base
{
    public interface IHasDeletionTime
    {
        public DateTime DeletionTime { get; set; }
    }
    public interface IHasDelet : IHasDeletionTime
    {
        public DateTime Deletion { get; set; }
    }
}
