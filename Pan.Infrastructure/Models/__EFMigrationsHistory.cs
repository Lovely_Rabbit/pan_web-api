﻿namespace Pan.Infrastructure.Models
{
    public partial class __EFMigrationsHistory
    {
        public string MigrationId { get; set; }
        public string ProductVersion { get; set; }
    }
}
