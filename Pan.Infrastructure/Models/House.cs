﻿using System;

namespace Pan.Infrastructure.Models
{
    public partial class house
    {
        public int id { get; set; }
        public int? city_id { get; set; }
        public int? area_id { get; set; }
        public int? house_id { get; set; }
        /// <summary>
        /// 备案名
        /// </summary>
        public string name { get; set; }
        /// <summary>
        /// 推广名
        /// </summary>
        public string promotion_name { get; set; }
        public string status { get; set; }
        public string avg_price { get; set; }
        public string address { get; set; }
        public string developer { get; set; }
        public string phone { get; set; }
        public string property { get; set; }
        /// <summary>
        /// 其他信息
        /// </summary>
        public string info { get; set; }
        public string real_data { get; set; }
        public DateTime? add_date { get; set; }
        public DateTime? update_date { get; set; }
        public string hash_id { get; set; }
        public int? keyland_id { get; set; }
    }
}
