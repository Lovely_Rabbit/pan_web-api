﻿using System;

namespace Pan.Infrastructure.Models
{
    public partial class queryparam
    {
        public int Id { get; set; }
        public string Query_param { get; set; }
        public int Query_count { get; set; }
        public int Status { get; set; }
        public DateTime? CreateOn { get; set; }
        public DateTime? UpdateOn { get; set; }
    }
}
