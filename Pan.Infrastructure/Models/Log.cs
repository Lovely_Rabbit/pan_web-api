﻿namespace Pan.Infrastructure.Models
{
    public partial class Log
    {
        public int Id { get; set; }
        public string request_id { get; set; }
        public string request_http { get; set; }
        public string request_url { get; set; }
        public string request_date { get; set; }
    }
}
