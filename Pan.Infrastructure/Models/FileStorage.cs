﻿using System;

namespace Pan.Infrastructure.Models
{
    public partial class FileStorage
    {
        public int Id { get; set; }
        public string FileExt { get; set; }
        public double FileSize { get; set; }
        public int FileType { get; set; }
        public string HashCode { get; set; }
        public string PartDir { get; set; }
        public string PathLocal { get; set; }
        public string Title { get; set; }
        public string OSSName { get; set; }
        public string OSSBucketName { get; set; }
        public string OSSETag { get; set; }
        public string OSSRequestId { get; set; }
        public int Status { get; set; }
        public DateTime CreateOn { get; set; }
        public DateTime? UpdateOn { get; set; }
    }
}
