﻿namespace Pan.Infrastructure.Models
{
    public partial class Course
    {
        public string c_id { get; set; }
        public string c_name { get; set; }
        public string t_id { get; set; }
    }
}
