﻿namespace Pan.Infrastructure.Models
{
    public partial class post_html
    {
        public long id { get; set; }
        public string html_context { get; set; }
        public int post_id { get; set; }
    }
}
