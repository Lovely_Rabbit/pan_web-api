﻿using System;

namespace Pan.Infrastructure.Models
{
    public partial class s_task_group
    {
        public int id { get; set; }
        public int city_id { get; set; }
        public int area_id { get; set; }
        public string name { get; set; }
        public DateTime add_date { get; set; }
        public long add_timestamp { get; set; }
    }
}
