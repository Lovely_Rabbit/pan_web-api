﻿namespace Pan.Infrastructure.Models
{
    public partial class STask
    {
        public int Id { get; set; }
        public int GroupId { get; set; }
        public string Name { get; set; }
        public string Info { get; set; }
        public int Type { get; set; }
        public int Pid { get; set; }
        public int Status { get; set; }
    }
}
