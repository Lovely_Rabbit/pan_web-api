﻿namespace Pan.Infrastructure.Models
{
    public partial class post_thread
    {
        public long id { get; set; }
        public string thread { get; set; }
        public int post_id { get; set; }
    }
}
