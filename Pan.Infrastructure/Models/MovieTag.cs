﻿namespace Pan.Infrastructure.Models
{
    public partial class MovieTag
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string category_title { get; set; }
    }
}
