﻿using System;

namespace Pan.Infrastructure.Models
{
    public partial class post
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string htmlContext { get; set; }
        public int? Tags { get; set; }
        /// <summary>
        /// 爬取来的id
        /// </summary>
        public string request_id { get; set; }
        /// <summary>
        /// 阿里云盘资源链接
        /// </summary>
        public string aliyun_url { get; set; }
        /// <summary>
        /// 详情的url
        /// </summary>
        public string del_url { get; set; }
        /// <summary>
        /// 下一页
        /// </summary>
        public string next_url { get; set; }
        public string prev { get; set; }
        public string createTime { get; set; }
        public int? Status { get; set; }
        public DateTime? CreateOn { get; set; }
        public DateTime? UpdateOn { get; set; }
        public string hash_id { get; set; }
        public string tags_title { get; set; }
        /// <summary>
        /// 封面图
        /// </summary>
        public string img { get; set; }
        public string video { get; set; }
        public string version { get; set; }
        public string info { get; set; }
        public string downld { get; set; }


        public string hash_v { get; set; }
    }
}
