﻿namespace Pan.Infrastructure.Models
{
    public partial class Movie
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? Tags { get; set; }
        /// <summary>
        /// 爬取来的id
        /// </summary>
        public string request_id { get; set; }
        /// <summary>
        /// 详情的url
        /// </summary>
        public string del_url { get; set; }
        /// <summary>
        /// 下一页
        /// </summary>
        public string next_url { get; set; }
        /// <summary>
        /// 电影subtitle
        /// </summary>
        public string subtitle { get; set; }
        /// <summary>
        /// 图片
        /// </summary>
        public string cover { get; set; }
        public string created_at { get; set; }
        public string Links { get; set; }
        public string Context { get; set; }
        public string hash_id { get; set; }
    }
}
