﻿using System.ComponentModel;

namespace Pan.Infrastructure.Enums
{
    public enum RedisCachePrefixEnums
    {
        /// <summary>
        /// 文章列表 Key 前缀
        /// </summary>
        [Description("Post:")]
        Post = 0,
        /// <summary>
        /// 文章详情 Key
        /// </summary>
        [Description("Detil:")]
        Detil = 1,

        [Description("{Job}:")]
        Job = 10,


        //[Description("{Job}:")]
        //InsertJob = 10,
    }
}
