﻿using System.ComponentModel;

namespace Pan.Infrastructure.Enums
{
    public enum EntityStatusEnums
    {
        [Description("删除")]
        Delete = -1,
        [Description("禁用")]
        Disable = 0,
        [Description("启用")]
        Normal = 1,
    }
}
