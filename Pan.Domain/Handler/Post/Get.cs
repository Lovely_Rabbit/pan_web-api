﻿using AutoMapper;
using MediatR;
using Pan.Domain.Caches.PostCacheService;
using Pan.Domain.Model.Common;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Post
{
    public class GetPostsHandler : IRequestHandler<PostsGetRequest, PostsGetResponse>
    {
        //private readonly PostCacheService _postCache;
        private readonly PostMdbCacheService _postMdbCacheService;
        public GetPostsHandler(PostMdbCacheService postMdbCacheService)
        {
            //_postCache = postCache;
            _postMdbCacheService = postMdbCacheService;
        }

        public async Task<PostsGetResponse> Handle(PostsGetRequest request, CancellationToken cancellationToken)
        {
            var item = await _postMdbCacheService.GetAsync(request.primaryKey);
            //  return await _postCache.GetAsync(request.primaryKey);
            return item;
        }
    }


    public class PostsGetRequest : IRequest<PostsGetResponse>
    {
        public string primaryKey { get; set; }
    }

    /// <summary>
    /// httpResponse
    /// </summary>
    public class PostsGetResponse : EntityDto<int>
    {
        public string Title { get; set; }
        public string[] htmlContext { get; set; }
        public string HashV { get; set; }
        public bool IsCache { get; set; } = false;
        public string Qq { get; set; } = "2764136779";
        public string Msg { get; set; } = "全部数据请联系！";
    }
}
