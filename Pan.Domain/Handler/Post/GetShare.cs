﻿using MediatR;
using Pan.Domain.Model.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Text.Json.Serialization;
using Pan.Domain.Caches.PostCacheService;
using Pan.Infrastructure.MongdbContext;
using Pan.Infrastructure.Models;
using Pan.Infrastructure.Elasticsearch;

namespace Pan.Domain.Handler.Post
{
    public class GetShareHandler : IRequestHandler<ShareIdRequest, HttpStatusCode>
    {
        private readonly PostShareCacheService _postShareCacheService;
        private readonly IMongoRepository<post> _mongoRepository;

        private readonly IElasticsearchClient _elasticsearch;

        public GetShareHandler(PostShareCacheService postShareCacheService, IElasticsearchClient elasticsearch, IMongoRepository<post> mongoRepository)
        {
            _elasticsearch = elasticsearch;
            _mongoRepository = mongoRepository;
            _postShareCacheService = postShareCacheService;
        }

        public async Task<HttpStatusCode> Handle(ShareIdRequest request, CancellationToken cancellationToken)
        {
           // var data = await _postShareCacheService.GetOrCreate(request.shareId);

            //var items =   await _mongoRepository.CountAsync(x=>x.Title.Contains("1"));
            return await _postShareCacheService.GetOrCreate(request.shareId);
        }
    }





    public class ShareIdRequest : IRequest<HttpStatusCode>
    {

        public string shareId { get; set; }
    }

    public class ShareIdResponse
    {

    }

}
