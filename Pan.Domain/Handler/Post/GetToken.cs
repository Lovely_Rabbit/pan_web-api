﻿using Amazon.Util.Internal;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.MongdbContext;
using SharpCompress.Readers;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Post
{
    #region 查询条件 参考 常见查询
    //    1、等于（Equal）
    //var filter = Builders<User>.Filter.Eq(u => u.Name, "John");

    //   2 Copy
    //    不等于（Not Equal）
    //var filter = Builders<User>.Filter.Ne(u => u.Name, "John");

    //  3  Copy
    //    大于（Greater Than）
    //var filter = Builders<User>.Filter.Gt(u => u.Age, 20);

    //    Copy
    //    大于等于（Greater Than or Equal）
    //var filter = Builders<User>.Filter.Gte(u => u.Age, 20);

    //    Copy
    //    小于（Less Than）
    //var filter = Builders<User>.Filter.Lt(u => u.Age, 30);

    //    Copy
    //    小于等于（Less Than or Equal）
    //var filter = Builders<User>.Filter.Lte(u => u.Age, 30);
    //    Copy
    //    在范围内（In）

    //var ages = new List<int> { 20, 25, 30 };
    //    var filter = Builders<User>.Filter.In(u => u.Age, ages);
    //    Copy
    //    不在范围内（Not In）

    //var ages = new List<int> { 20, 25, 30 };
    //    var filter = Builders<User>.Filter.Nin(u => u.Age, ages);
    //    Copy
    //    组合条件（And）

    //var filter = Builders<User>.Filter.And(
    //    Builders<User>.Filter.Gt(u => u.Age, 20),
    //    Builders<User>.Filter.Eq(u => u.Name, "John")
    //);
    //    Copy
    //    组合条件（Or）

    //var filter = Builders<User>.Filter.Or(
    //    Builders<User>.Filter.Eq(u => u.Name, "John"),
    //    Builders<User>.Filter.Eq(u => u.Name, "Jane")
    //);

    //包含子字符串（不区分大小写）
    //var filter = Builders<User>.Filter.Regex(u => u.Name, new BsonRegularExpression("John", "i"));

    #endregion
    public class GetTokenHandler : IRequestHandler<GetTokenRequest, PagedResultDto<PostItems>>
    {

        private readonly IMongoRepository<PostItems> _mongoRepository;
        public GetTokenHandler(IMongoRepository<PostItems> mongoRepository)
        {
            _mongoRepository = mongoRepository;
        }
        public async Task<PagedResultDto<PostItems>> Handle(GetTokenRequest request, CancellationToken cancellationToken)
        {

            var filter = Builders<PostItems>.Filter.And(
                Builders<PostItems>.Filter.Regex(u => u.Title,
                new BsonRegularExpression(Regex.Escape(request.Title), "i"))
                // ,
                //Builders<PostItems>.Filter.Regex(u => u.htmlContext,
                // new BsonRegularExpression(Regex.Escape(request.Title), "i"))
                );




            string[] fields = { "Title", "id", "tags_title", "CreateOn", "HashV" };
            var sortDeftion = Builders<PostItems>.Sort.Descending(x => x.CreateOn);

            var items = await _mongoRepository.FindListByPageAsync(filter, request.SkipCount
                , request.MaxResultCount, fields, sortDeftion)
                ;

            return new PagedResultDto<PostItems>(items, items.Count);
        }
    }



    public class GetTokenRequest : PagedAndSortedRequest, IRequest<PagedResultDto<PostItems>>
    {
        public string Title { get; set; }
        public override int SkipCount
        {
            get => base.SkipCount = base.SkipCount == 0 ? 1 : base.SkipCount; set
            => base.MaxResultCount = value;
        }

        //public override int MaxResultCount { get => base.MaxResultCount; set => base.MaxResultCount = value; }
    }

    public class GetTokenReponse
    {

    }
}
