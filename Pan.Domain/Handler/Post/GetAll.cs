﻿using MediatR;
using Nest;
using Pan.Domain.Caches;
using Pan.Domain.Caches.PostCacheService;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Elasticsearch;
using Pan.Infrastructure.EnumExceptions;
using Pan.Infrastructure.Enums;
using Pan.Infrastructure.Exceptions;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pan.Domain.Handler.Post
{
    public class PostHandler : IRequestHandler<PostRequest, PagedResultDto<PostItems>>
    {
        private readonly string Prefix = RedisCachePrefixEnums.Post.GetDescriptionByEnum();
        private readonly PostAllCacheService _postCache;

        private readonly PostAllMdbCacheService _postAllCacheService;
        public PostHandler(PostAllCacheService postCache, PostAllMdbCacheService postAllCacheService)
        {

            _postCache = postCache;
            _postAllCacheService = postAllCacheService;
        }

        //private async IAsyncEnumerable<KeyValuePair<string, string>> GetHashData(int pageSize)
        //{
        //    IAsyncEnumerable<HashEntry> items = RedisCache.HashScanAsync("Post:IkTitle");
        //    int count = 0;
        //    //var q = ((IScanningCursor)items).Cursor;
        //    await foreach (var entry in items)
        //    {
        //        yield return new KeyValuePair<string, string>(entry.Name, entry.Value);
        //        count++;
        //        if (count == pageSize)
        //        {
        //            break;
        //        }
        //    }
        //}

        public async Task<PagedResultDto<PostItems>> Handle(PostRequest request, CancellationToken cancellationToken)
        {
            if (request.MaxResultCount != 40)
                throw new MaxResultPageMaxException();
            if (request.SkipCount > 1)
                throw new MaxResultPageSkipException();
            var keyStr = string.Empty;
            keyStr = string.IsNullOrEmpty(request.Keyword) == true ?
                keyStr = Prefix + request.SkipCount : Prefix + request.Keyword;

            //var dic = new Dictionary<string, string>();
            //await foreach (var item in GetHashData(50))
            //{
            //    dic.Add(item.Key, item.Value);
            //}
            //var q = await _postCache.GetOrCreate(keyStr);
            return await _postAllCacheService.GetOrCreate(keyStr);
        }
    }

    public class PostRequest : PagedAndSortedRequest, MediatR.IRequest<PagedResultDto<PostItems>>
    {
        public string Keyword { get; set; }
    }

    //[BsonCollection("post")]
    //[BsonFactoryMethod("post")]
    [Table("post")]
    public class PostItems : EntityDto<int>
    {
        /// <summary>
        /// hashId
        /// </summary>
        /// 
        [BsonElement("HashV")]
        public string PrimaryKey { get; set; }
        //public string HashV { get; set; }
        public string Title { get; set; }

        public bool IsCache { get; set; } = false;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateOn { get; set; }

        //public string[] AlyUrl { get; set; }
        [BsonElement("tags_title")]
        public string TagName { get; set; }
        public string Qq { get; set; } = "2764136779";
        public string Msg { get; set; } = "全部数据请联系！";
    }


}