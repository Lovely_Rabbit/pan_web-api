﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Pan.Common.Helper.TagDictionary;
using Pan.Domain.Caches.MemoryCache;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Series
{
    internal class SeriesHandler : IRequestHandler<SeriesRequest, PagedResultDto<SeriesResponse>>
    {
        private readonly IRepository<post_thread> _threadRepository;

        public SeriesHandler(IRepository<post_thread> threadRepository)
        {
            _threadRepository = threadRepository;
        }

        public async Task<PagedResultDto<SeriesResponse>> Handle(SeriesRequest request, CancellationToken cancellationToken)
        {
            if (request.MaxResultCount != 40)
                throw new MaxResultPageMaxException();
            if (request.SkipCount > 0)
                throw new MaxResultPageSkipException();
            var items = new List<SeriesResponse>();


            //查询 状态为 0  url不是空的
            var tagsItems = TagDictionary.Tagsdictionary();
            var lableitems = await _threadRepository.GetQueryWithDisable()
                    .Where(x => x.IsUpdate == true)
                    .Where(x => x.AliyunpanUrl != "")
                    .Where(x => tagsItems.Values.Contains(x.TagName))
                    .Skip(request.SkipCount).Take(request.MaxResultCount).ToListAsync();
            //DateTimeOffset dto = new DateTimeOffset(DateTime.Now);
            //var unixTime = dto.ToUnixTimeSeconds();
            //DateTime dtime = new DateTime();
            //dtime.AddDays(-2);
            Random rd = new Random();
            //RondowExtensions.NextDouble()
            foreach (var node in tagsItems)
            {
                var dbrnd = rd.NextDouble(-6, -1);
                //var lableitem = new List<LableItems>();
                var lableitem = await _threadRepository.GetQueryWithDisable()
                    .Where(x => x.TagName == node.Value)
                    .Where(x => x.IsUpdate == true)
                    .Where(x => x.AliyunpanUrl != "")
                    .Where(x => x.CreateOn.Value >= DateTime.Now.AddDays(dbrnd))
                    .OrderByDescending(x => x.CreateOn)
                    //.ThenBy(x => Guid.NewGuid())
                    .Select(q => new LableItems
                    {
                        Url = q.AliyunpanUrl,
                        Title = q.thread,
                    }).Skip(request.SkipCount).Take(request.MaxResultCount).ToListAsync();
                lableitem.ForEach(x =>
                {
                    x.Url = x.Url.TrimEnd(new char[] { ',' }).Split(",")[0];
                });

                items.Add(new SeriesResponse
                {
                    Name = node.Value,
                    CreateOn = DateTime.Now,
                    Lable = lableitem
                });
            }
            var query = items.Select(r =>
            {
                var data = new SeriesResponse();
                data.Lable = r.Lable;
                data.Name = r.Name;
                data.CreateOn = r.CreateOn;

                return data;
            });
            return new PagedResultDto<SeriesResponse>(query, 0);
        }
    }

    public class SeriesRequest : PagedRequest, IRequest<PagedResultDto<SeriesResponse>>
    {

    }

    public class SeriesResponse
    {
        public string Name { get; set; }

        public List<LableItems> Lable { get; set; }

        public DateTime? CreateOn { get; set; }
    }

    public class LableItems : EntityDto<int>
    {
        public string Title { get; set; }

        public string Url { get; set; }
    }
}