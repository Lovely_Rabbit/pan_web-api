﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Pan.Common.Encryptions;
using Pan.Domain.Caches;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.Enums;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.TaskJob.JobCache
{
    public class KeyItems
    {
        public string Key { get; set; }
        public RedisItems item { get; set; }
    }
    public class RedisItems
    {
        public int Id { get; set; }
        public string HashId { get; set; }

        public string Title { get; set; }

        public string AliUrl { get; set; }

        public string RequestId { get; set; }

        public string TagsTitle { get; set; }

        public int Tags { get; set; }
    }

    /// <summary>
    /// 查询redis 里面hash 新增的数据，同步到数据表中
    /// </summary>
    public class TableJobHandler : IRequestHandler<TableRequest, bool>
    {
        private readonly IRepository<post_thread> _threadRepository;
        private readonly ILogger<TableJobHandler> _logger;
        private readonly IRepository<post_html> _htmlRepository;
        //private readonly IRepository<post> _postRepository;
        protected readonly EFCoreDbContext _dbContext;


        public TableJobHandler(IRepository<post_thread> threadRepository, IRepository<post_html> htmlRepository
            // , IRepository<post> postRepository
            , ILogger<TableJobHandler> logger,
            EFCoreDbContext dbContext
            )
        {
            _dbContext = dbContext;
            _logger = logger;
            _htmlRepository = htmlRepository;
            _threadRepository = threadRepository;
            //_postRepository = postRepository;
        }


        public async Task<bool> Handle(TableRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var islock = RedisCache.LockTake(request.parmKey, request.parmKey);
                if (!islock) return await Task.FromResult(true); //加锁
                if (!await RedisCache.ExistsAsync(request.parmKey, 4)) return await Task.FromResult(true);
                var dicredisData = RedisCache.GetDatabase(4)
                    .HashScan(request.parmKey).Take(request.pageSize).Select(q => new KeyItems
                    {
                        Key = q.Name,
                        item = JsonConvert.DeserializeObject<RedisItems>(q.Value)

                    }).ToDictionary(k => k.Key, v => v.item);
                if (!dicredisData.Any())
                    return true;

                //查询数据库已经有的
                var keys = dicredisData.Select(q => q.Key).ToList();

                var anyItems = await _threadRepository.GetQueryWithDelete()
                    .Where(x => keys.Contains(x.HashId))
                    .ToListAsync();
                //需要写入的数据 查询和redis中的数据差异

                var createItems = dicredisData.Values
                    .Where(x => !anyItems.Any(q => x.HashId == q.HashId))
                    .Select(r => new post
                    {
                        Tags = r.Tags,
                        aliyun_url = r.AliUrl,
                        request_id = r.RequestId,
                        tags_title = r.TagsTitle,
                        Title = r.Title,
                        hash_id = r.HashId,
                        del_url = "",
                        HashV = MD5Encryption.ComputeHash(r.HashId),
                    })
                    .ToList();

                var threadItems = createItems.Select(r => new post_thread
                {
                    thread = r.Title,
                    AliyunpanUrl = r.aliyun_url,
                    TagName = r.tags_title,
                    HashId = r.hash_id,
                    Status = EntityStatusEnums.Normal,
                    HashV = MD5Encryption.ComputeHash(r.hash_id),

                });
                var thml = createItems.Select(r => new post_html
                {
                    hash_id = r.hash_id,
                    html_context = r.aliyun_url,
                    HashV = MD5Encryption.ComputeHash(r.hash_id),
                });
                if (createItems.Any() || threadItems.Any())
                {
                    //写入数据
                    await _htmlRepository.AddRangeAsync(thml);
                    await _threadRepository.AddRangeAsync(threadItems);
                    await _threadRepository.SaveChangesAsync();
                }
                List<RedisValue> redisValues = new List<RedisValue>();
                foreach (var item in keys) redisValues.Add(item);
                var delint = await RedisCache.HashDeleteAsync(request.parmKey, dataKeys: redisValues, 4);
                _logger.LogInformation("开始处理定时任务 {CommandName} ({@Command})", request.parmKey, request);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message, "处理定时任务出错 {CommandName} ({@Command})", request.parmKey, request);
                throw new Exception(ex.Message);
            }
            finally
            {
                RedisCache.LockRelease(request.parmKey, request.parmKey); //释放锁
            }


        }

    }

    public class TableRequest : IRequest<bool>
    {
        public int pageSize { get; set; }

        public string parmKey { get; set; }
    }


    //Update
    public class UpdateItems
    {
        /// <summary>
        /// 
        /// </summary>
        public int? Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string HashId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AliUrl { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RequestId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int Tags { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TagsTitle { get; set; }
    }


}
