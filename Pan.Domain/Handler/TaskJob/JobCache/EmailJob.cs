﻿using MediatR;
using Newtonsoft.Json;
using Pan.Common.Helper.Email;
using Pan.Domain.Caches;
using Pan.Infrastructure.EnumExceptions;
using Pan.Infrastructure.Enums;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.TaskJob.JobCache
{
    public class EmailJobHandler : IRequestHandler<EmailJobRequest, bool>
    {
        private readonly string Prefix = RedisCachePrefixEnums.Post.GetDescriptionByEnum();
        private readonly string PrefixJob = RedisCachePrefixEnums.Job.GetDescriptionByEnum();
        private readonly Regex EmailRegex = new(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", RegexOptions.Compiled);
        public Task<bool> Handle(EmailJobRequest request, CancellationToken cancellationToken)
        {
            //消费redis
            Action<RedisChannel, RedisValue> csBlack = EmailBack;
            RedisCache.SubscribeBlack(PrefixJob + Prefix, csBlack);
            //var str = await RedisCache.HashGetAllAsync(PrefixJob, (int)RedisCachePrefixEnums.Job);
            //if (str.Length == 0)
            //    return false;

            return Task.FromResult(true);
        }

        public void EmailBack(RedisChannel key, RedisValue value)
        {
            //解析 value
            //value = value.ToString().Replace('\"',' ');
            //value = value.string
            //Console.WriteLine("{0}当前的邮箱", value.ToString());
            //反序列化
            var emailmsg = JsonConvert.DeserializeObject<EmailMsg>(value);
            StringBuilder content = new StringBuilder();
            content.Append("文章预加载写入完成！");
            content.Append("<br>" + emailmsg.Messg);
            var css = new List<string> { "" };
            var bccs = new List<string> { "" };
            //var content = "测试 邮件发送！";
            var title = string.Format("您好:{0}文章预加载任务写入完成!", emailmsg.Email);
            var isSend = EmailHelper.SendMail(emailmsg.Email, css, bccs, content.ToString(), title);
            //Console.WriteLine("回调后的消息:{0}", value);
        }
    }

    public class EmailJobRequest : IRequest<bool>
    {

        /// <summary>
        /// 邮箱地址 可以多填
        /// </summary>
        // [EmailAddress(ErrorMessage = "{0}格式不正确！")]
        //public IList<string> Email { get; set; }
    }
}
