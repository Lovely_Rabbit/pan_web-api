﻿using MediatR;
using Nest;
using Pan.Domain.Caches;
using Pan.Infrastructure.Elasticsearch;
using Pan.Infrastructure.MongdbContext;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using static System.Net.Mime.MediaTypeNames;

namespace Pan.Domain.Handler.TaskJob.JobCace.Hahsh
{
    public class HashVSubHandler : IRequestHandler<HashVSubRequest, bool>
    {
        private readonly IMongoContext _mongdbPostContext;
        private readonly IElasticsearchClient _elasticsearch;
        private async IAsyncEnumerable<KeyValuePair<string, string>> GetHashData(int pageSize)
        {
            IAsyncEnumerable<HashEntry> items = RedisCache.HashScanAsync("Post:IkTitle");
            int count = 0;
            //var q = ((IScanningCursor)items).Cursor;
            await foreach (var entry in items)
            {
                yield return new KeyValuePair<string, string>(entry.Name, entry.Value);
                count++;
                if (count == pageSize)
                {
                    break;
                }
            }
        }
        public HashVSubHandler(IMongoContext mongdbPostContext, IElasticsearchClient elasticsearchClient)
        {
            _elasticsearch = elasticsearchClient;
            _mongdbPostContext = mongdbPostContext;
        }

        public async Task<bool> Handle(HashVSubRequest request, CancellationToken cancellationToken)
        {
            try
            {
                var islock = RedisCache.LockTake(request.lOCK, request.lOCK);
                if (!islock) return true; //
                var iskey = RedisCache.Exists("Post:IkTitle", 0);
                if (!iskey) return true;
                var redisKey = new List<RedisValue>();
                var dic = new Dictionary<string, List<string>>();
                var data = new List<HashItems>();
                await foreach (var item in GetHashData(request.pageSize))
                {
                    IReadOnlyCollection<AnalyzeToken> text = await _elasticsearch.AnalyzerIkAsync(item.Value);
                    var analy = text.Select(x => x.Token).ToList();
                    foreach (var v in analy)
                    {
                        data.Add(new HashItems
                        {
                            Idkey = item.Key,
                            values = v
                        });

                    }
                    redisKey.Add(item: item.Key);
                    //  dic.Add(item.Key, analy);
                }

                await _mongdbPostContext.GetCollection<HashItems>("IKpost").InsertManyAsync(data);
                var result = await RedisCache.HashDeleteAsync("Post:IkTitle", redisKey, 0);
                return result > 0 ? true : false;

            }
            catch (Exception)
            {

            }
            finally
            {
                RedisCache.LockRelease(request.lOCK, request.lOCK); //释放锁
            }
            return false;
        }
    }

    public class HashVSubRequest : MediatR.IRequest<bool>
    {
        public string lOCK { get; set; } = "HashVSubRequest";

        public int pageSize { get; set; }
    }


    public class HashItems //: AnalyzeToken
    {
        public string Idkey { get; set; }

        public string values { get; set; }
    }
}
