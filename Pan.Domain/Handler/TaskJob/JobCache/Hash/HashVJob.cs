﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using MongoDB.Bson;
using MongoDB.Driver;
using Pan.Common.Encryptions;
using Pan.Domain.Caches;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.Enums;
using Pan.Infrastructure.Extensions;
using Pan.Infrastructure.MongdbContext;
using SharpCompress.Common;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static RabbitMQ.Client.Logging.RabbitMqClientEventSource;

namespace Pan.Domain.Handler.TaskJob.JobCache.Hash
{
    public class HashVJob : IRequestHandler<HashVJobRequest, bool>
    {
        private readonly IRepository<post> _postrepository;
        protected readonly EFCoreDbContext _dbContext;
        private readonly IMongoContext _mongdbPostContext;

        public HashVJob(EFCoreDbContext dbContext, IMongoContext mongdbPostContext, IRepository<post> postrepository)
        {
            _mongdbPostContext = mongdbPostContext;
            _postrepository = postrepository;
            _dbContext = dbContext;
        }

        public async Task<bool> Handle(HashVJobRequest request, CancellationToken cancellationToken)
        {
            try
            {

                var islock = RedisCache.LockTake(request.lOCK, request.lOCK);
                if (!islock) return true; //加锁
                int postmaxId = 0;
                var intv = RedisCache.Get<int>("pageIncrement");

                postmaxId = RedisCache.Get<int>("pageMax");


                //获取 mongdbmaxid
                var pipeline = new BsonDocument[]
                {
                    new BsonDocument("$group", new BsonDocument
                    {
                        { "_id", "" },
                        { "maxId", new BsonDocument("$max", "$_id") }
                    })
                };
                if (!RedisCache.Exists("pageMax") && postmaxId == 0)
                {
                    // var querymaxid = _postrepository.GetQueryWithDelete().Max(x => x.Id);
                    // 执行聚合查询
                    var result = await _mongdbPostContext.GetCollection<post>("post")
                        .Aggregate<BsonDocument>(pipeline).FirstOrDefaultAsync();

                    // 获取最大值
                    postmaxId = result["maxId"].AsInt32;
                    RedisCache.Set("pageMax", postmaxId, DateTime.Now.AddHours(0.5), 0);
                }



                var query = _postrepository.GetQueryWithDelete()
                    //.WhereIf(postMinId.HasValue, x => x.Id < postMinId)
                    .WhereIf(postmaxId != 0, x => x.Id > postmaxId)
                    ;

                var items = await query.OrderAndPagedAsync(request);
                var arrys = items.Select(r => new HashEntry(r.Id, r.Title)).ToArray();
                if (!arrys.Any()) return true;

                var postMaxid = arrys.Select(r => r.Name).Min();

                //RedisCache.Set("pageNumber", ((int)postMaxid), DateTime.Now.AddSeconds(20), 0);
                if (postMaxid == postmaxId)
                {
                    RedisCache.PointIncrement1("pageIncrement", 1);
                }

                //推送到redis 进行数据分词


                await RedisCache.HashSetAsync("Post:IkTitle", arrys);

                var existingIds = _mongdbPostContext.GetCollection<post>("post")
                    .Find(Builders<post>
                    .Filter.In(field: d => d.Id, items.Select(d => d.Id))).ToList()
                    .Select(d => d.Id).ToHashSet();
                var newData = items.Where(d => !existingIds.Contains(d.Id)).ToList();
                if (newData.Any())
                {

                    await _mongdbPostContext.GetCollection<post>("post").InsertManyAsync(newData);
                    //_logger.LogInformation($"同步了 {newData.Count} 条新数据到 MongoDB。");
                }
                else
                {
                    //_logger.LogInformation("没有新数据需要同步。");
                }




            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);

            }
            finally
            {
                RedisCache.LockRelease(request.lOCK, request.lOCK); //释放锁
            }

            return true;
        }
    }


    public class HashVJobRequest : PagedAndSortedIdRequest, IRequest<bool>
    {
        public string lOCK { get; set; } = "md5jobkey";
        /// <summary>
        /// 邮箱地址 可以多填
        /// </summary>
        // [EmailAddress(ErrorMessage = "{0}格式不正确！")]
        // public IList<string> Email { get; set; }

        public override int MaxResultCount { get; set; } = 100;
    }

    public class KeysMongdb
    {
        public int key { get; set; }

        public string values { get; set; }
    }

    public class HashDto
    {
        public string Hash_id;
        public string Hashv;
        public int Id;

        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime? CreateOn { get; set; }

        /// <summary>
        /// 更新时间
        /// </summary>
        public DateTime? UpdateOn { get; set; }

        public EntityStatusEnums Status { get; set; }
    }
}
