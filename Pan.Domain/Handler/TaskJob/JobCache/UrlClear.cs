﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Pan.Domain.Caches;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.TaskJob.JobCache
{
    public class UrlClearHandler : IRequestHandler<UrlRequest, bool>
    {

        private readonly IRepository<post> _postrepository;
        private readonly IRepository<VerifyUrl> _verirepository;
        private readonly ILogger<UrlClearHandler> _logger;
        protected readonly EFCoreDbContext _dbContext;
        public UrlClearHandler(IRepository<post> repository, IRepository<VerifyUrl> verirepository, ILogger<UrlClearHandler> logger, EFCoreDbContext dbContext)
        {
            _logger = logger;
            _postrepository = repository;
            _verirepository = verirepository;
            _dbContext = dbContext;
        }

        public async Task<bool> Handle(UrlRequest request, CancellationToken cancellationToken)
        {
            var updateKeys = new Dictionary<string, VerifyUrl>();
            var deleKeys = new Dictionary<post, string>();

            try
            {
                var islock = RedisCache.LockTake(request.lOCK, request.lOCK);
                if (!islock) return true; //加锁
                var query = _postrepository.GetQueryWithDisable()
                    .Where(q => q.IsUpdate == 0)
                    .Where(q => q.aliyun_url != "")
                   .Select(q => new
                   {
                       q.Id,
                       q.aliyun_url,
                       q.CreateOn
                   })
                    ;
                request.MaxResultCount = request.pageCount;
                var items = await query.OrderAndPagedAsync(request)
                    ;
                if (!items.Any()) return await Task.FromResult(true);

                var dic = items
                    .ToDictionary(k => k.Id,
                    v => v.aliyun_url);

                foreach (var item in dic)
                {
                    var shareIds = item.Value.Split(",").Select(q =>
                    {
                        return q.Replace("https://www.alipan.com/s/", "").Replace("https://www.aliyundrive.com/s/", "");
                    });

                    foreach (var shareId in shareIds.Where(q => q != "").Distinct())
                    {
                        if (!updateKeys.ContainsKey(shareId))
                        {
                            var veriItme = new VerifyUrl
                            {
                                PostId = item.Key,
                                ShareId = shareId,
                            };
                            updateKeys.Add(shareId, veriItme);
                        }
                        else
                        {
                            deleKeys.TryAdd(new post { Id = item.Key }, shareId);
                        }
                    }
                }
                //新增 verifyUrl 
                await _verirepository.AddRangeAsync(updateKeys.Values);

                _postrepository.HardDeleteRange(deleKeys.Keys);
                await _postrepository.SaveChangesAsync();
                //合并
                var jsonKeys = string.Join(",", updateKeys.Values.Select(x => x.PostId).Distinct());
                FormattableString SQL = $"UPDATE post SET `IsUpdate` = 1 WHERE Id in ({jsonKeys})";
                var upItems = _postrepository.GetQueryFromSql(SQL).ToString();
                //do
                //{
                //    //当前为定时任务，不需要此操作
                //} while (true);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                var jsonKeys = string.Join(",", updateKeys.Values.Select(x => x.PostId).Distinct());
                FormattableString SQL = $"UPDATE post SET `IsUpdate` = 0 WHERE Id in ({jsonKeys})";
                var upItems = _postrepository.GetQueryFromSql(SQL).ToString();
                throw new DbUpdateConcurrencyException("urlClear 并发");
            }
            finally
            {
                RedisCache.LockRelease(request.lOCK, request.lOCK); //释放锁
            }

            //foreach (var item in items)
            //{
            //    var arry = item.aliyun_url.Split(",");
            //    for (int i = 0; i < arry.Length - 1; i++)
            //    {
            //        var shareId = arry[i].Replace("https://www.aliyundrive.com/s/", "");
            //        var result = await VerifyUrl(shareId);
            //        if (result == HttpStatusCode.OK)
            //        {
            //            //修改
            //            updateKey += item.Id.ToString() + ",";
            //        }
            //        if (result == HttpStatusCode.BadRequest && !idDic.ContainsKey(item.Id))
            //        {
            //            Console.WriteLine(shareId);
            //            idDic.Add(item.Id, item.Id.ToString());
            //            updateIds += item.Id.ToString() + ",";
            //        }
            //        //修改状态 status = 1
            //    }
            //}
            //#region 数据库版修改
            //if (updateKey.Any())
            //{
            //    var t = updateKey.Substring(0, updateKey.Length - 1);
            //    var upItems = await _dbContext.Database.ExecuteSqlRawAsync($"UPDATE post SET `IsUpdate` = 1 WHERE Id in ({t})");
            //}
            //if (updateIds.Any())
            //{
            //    try
            //    {
            //        Console.WriteLine(updateIds.Length);
            //        var t = updateIds.Substring(0, updateIds.Length - 1);
            //        var upItems = await _dbContext.Database.ExecuteSqlRawAsync($"UPDATE post SET `Status` = 1 WHERE Id in ({t})");
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine(ex.Message);
            //        throw;
            //    }
            //}
            // #endregion


            // redis 版 stream  订阅

            return true;
        }


        private async Task<HttpStatusCode> VerifyUrl(string share_id)
        {
            using (HttpClient httpClient = new HttpClient())
            {
                //{\"share_id\":\"{0}\"}
                httpClient.Timeout = TimeSpan.FromSeconds(10);
                string paly = "{\"share_id\":\"" + share_id + "\"}";
                using var httcontent = new StringContent(paly);
                httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                var rep = await httpClient.PostAsync($"https://api.aliyundrive.com/adrive/v3/share_link/get_share_by_anonymous?share_id={share_id}"
                    , httcontent);

                string reponseBody = await rep.Content.ReadAsStringAsync();
                return rep.StatusCode;

            }
        }
    }


    public class UrlRequest : PagedAndSortedRequest, IRequest<bool>
    {
        public int pageCount { get; set; }

        public string lOCK { get; set; } = "URLCLEAR";
    }
}
