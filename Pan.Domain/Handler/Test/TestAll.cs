﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Pan.Domain.Caches.MemoryCache;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Test
{
    public class TestSubscribeHandler : IRequestHandler<SubscribeGetAllRequest, bool>
    {
        public delegate void MsgDelegate(string message);

        public event MsgDelegate Msg;

        // private readonly IRabbitMQ _rabbitMQ;
        private readonly IRepository<post_thread> _postThreadRepository;

        private readonly IMemoryCacheHelper _memoryCacheHelper;

        public TestSubscribeHandler(
             //IRabbitMQ rabbitMQ
             IMemoryCacheHelper memoryCacheHelper
             , IRepository<post_thread> postThreadRepository)
        {
            _memoryCacheHelper = memoryCacheHelper;
            _postThreadRepository = postThreadRepository;
            // _rabbitMQ = rabbitMQ;
        }

        public void Val(string obj)
        {
            Console.WriteLine($"回调的：{obj}");
        }

        //public string Val2(string obj)
        //{
        //    var data = JsonConvert.DeserializeObject<CachGetResponse>(obj);
        //    //update 修改 aliyun url
        //    if (data == null)
        //        throw new MQUpdateExceptionin();
        //    var query = _postThreadRepository.GetQueryWithDelete()
        //         .FirstOrDefault(x => x.post_id == data.post_id);
        //    var datas = query;
        //    return "";
        //}

        public async Task<bool> Handle(SubscribeGetAllRequest request, CancellationToken cancellationToken)
        {
            //_rabbitMQ.CreateConsumer()
            //获取MQ 的消息

            // Action<string> messageTarget = Val;
            //_rabbitMQ.CreateConsumer("exchange", "", "postHtml", messageTarget, ExchangeType.Direct, null);

            //messageTarget("a");
            //Action<string> messageTarget = null;
            //var upd = new UpdaMq();
            //await Task.WhenAll(Task.Run(() =>
            //{
            //    messageTarget = upd.Val;
            //}));

            //Msg = Val;
            //Func<string, string> tt2 = new Func<string, string>(Val2);
            //tt2("");
            //var tt = await _postThreadRepository.GetQueryWithDelete().FirstOrDefaultAsync();
            //本月
            var data = await _postThreadRepository.GetQueryWithDelete()
                .Where(q => q.AliyunpanUrl != "" || q.AliyunpanUrl != null)
                .Where(q => q.CreateOn > DateTime.Now.AddDays(
                    -Convert.ToInt32(DateTime.Now.Date.Day)
                    ))
                .Skip(0).Take(2)
                .Select(r => new GetAllResponseItems
                {
                    //post_id = r.post_id,
                    AliyunpanUrl = r.AliyunpanUrl,
                    thread = r.thread,
                }).ToListAsync();
            //var items = await _postThreadRepository.GetQueryWithDelete()
            //    .Where(q => q.AliyunpanUrl != "" || q.AliyunpanUrl != null)
            //    .Skip(0).Take(10)
            //    .Select(r => new GetAllResponseItems
            //    {
            //        post_id = r.post_id,
            //        AliyunpanUrl = r.AliyunpanUrl,
            //        thread = r.thread,
            //    }).ToListAsync();

            //List<string> cacheKeys = _memoryCacheHelper.GetAllKeys();
            //foreach (var item in cacheKeys)
            //{
            //    RedisCache.LeftPush("test", item, 2);
            //}
            ////Func<string> messg = () => Val;
            ////var strs = new List<string>();
            //foreach (var item in data)
            //{
            //    var json = JsonConvert.SerializeObject(item);
            //    //strs.Add(json);
            //    RedisCache.LeftPush("listPost", json);
            //}
            //_memoryCacheHelper.Dispose();
            //RedisCache.LeftPushByList("listPost", strs);
            //每次 偏移 一个数据
            // await _rabbitMQ.CreateConsumer2("exchange", "", "test", messg, ExchangeType.Direct, null);

            // await _rabbitMQ.CreateConsumer2("exchange", "", "postHtml", tt2, ExchangeType.Direct, null);
            // var msg = messageTarget.Invoke;

            return true;
        }
    }

    public class UpdaMq
    {
        private readonly IRepository<post_thread> _postThreadRepository;

        public UpdaMq()
        {
        }

        public UpdaMq(IRepository<post_thread> postThreadRepository)
        {
            _postThreadRepository = postThreadRepository;
        }

        public void Val(string obj)
        {
            var data = JsonConvert.DeserializeObject<CachGetResponse>(obj);
            //update 修改 aliyun url
            if (data == null)
                throw new MQUpdateExceptionin();
            //var query = _postThreadRepository.GetQueryWithDelete()
            //     .FirstOrDefault(x => x.post_id == data.post_id);
            //var datas = query;
        }
    }

    public class SubscribeGetAllRequest : IRequest<bool>
    {
    }

    public class GetAllResponseItems
    {
        public int post_id { get; set; }
        public string AliyunpanUrl { get; set; }
        public string thread { get; set; }
    }
}