﻿using Pan.Common.MQ;
using System;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Test
{
    public class TestHandler : MQEventHandler<CachGetResponse>
    {

        /// <summary>
        /// 收到消息
        /// </summary>
        /// <param name="data"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        public override Task OnReceivedAsync(CachGetResponse data, string message)
        {

            Console.WriteLine("回调后的参数：{0}", message);

            return Task.CompletedTask;
        }
    }
}
