﻿using MediatR;
using Pan.Common.Helper.Email;
using Pan.Infrastructure.Exceptions;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Test
{
    public class TestEmailHandler : IRequestHandler<TestEmailRequest, bool>
    {
        private readonly Regex EmailRegex = new(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", RegexOptions.Compiled);

        public Task<bool> Handle(TestEmailRequest request, CancellationToken cancellationToken)
        {
            if (request.Email == null || request.Email.Count < 0)
                throw new ValidatorException("邮件配置错误！11");

            foreach (var item in request.Email)
            {
                if (!EmailRegex.IsMatch(item))
                {
                    throw new ValidatorException("邮件配置错误！");
                }
            }

            //var strto = "3126895987@qq.com";
            var css = new List<string> { "" };
            var bccs = new List<string> { "" };
            var content = "测试 邮件发送！";
            var title = "测试发送标题！";

            var isSend = EmailHelper.SendMail(request.Email, css, bccs, content, title);

            return Task.FromResult(isSend);
        }
    }

    public class TestEmailRequest : IRequest<bool>
    {
        //private readonly static Regex EmailRegex = new(@"^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$", RegexOptions.Compiled);
        /// <summary>
        /// 邮箱地址 可以多填
        /// </summary>
       // [EmailAddress(ErrorMessage = "{0}格式不正确！")]
        public IList<string> Email { get; set; }

        /// <summary>
        /// 判断邮箱
        /// </summary>
        /// <param name="emails"></param>
        //public void IsEmail(IList<string> emails)
        //{
        //    foreach (var item in emails)
        //    {
        //        if (EmailRegex.IsMatch(item))
        //        {

        //        }
        //    }
        //}
    }
}


