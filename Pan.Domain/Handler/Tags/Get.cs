﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Handler.Tags
{

    public class TagsTitleGetHandler : IRequestHandler<TagsTitleGetRequest, PagedResultDto<TagsTitleGetResponse>>
    {
        private readonly IRepository<Tag> _tagRepository;
        private readonly IMapper _mapper;


        public TagsTitleGetHandler(IRepository<Tag> tagRepository
            , IMapper mapper

            )
        {
            _tagRepository = tagRepository;
            _mapper = mapper;

        }

        public async Task<PagedResultDto<TagsTitleGetResponse>> Handle(TagsTitleGetRequest request, CancellationToken cancellationToken)
        {
            //string tagcache = _redis.StringGet("tags");
            //if (!string.IsNullOrEmpty(tagcache))
            //{
            //   // var objs = _redis.StringGet("tags");
            //    var item = JsonConvert.DeserializeObject<PagedResultDto<TagsTitleGetResponse>>(objs);
            //    return item;
            //    //return new PagedResultDto<TagsTitleGetResponse>(null, 0); ;
            //}
            var query = _tagRepository.GetQueryWithDelete().OrderBy(x => x.Id);
            var count = await query.CountAsync();
            var data = await query.ToListAsync();
            // var fdata = data.Where(x => x.child_Tag != 0); //子标签
            var items = data.Select(x =>
              {
                  var item = _mapper.Map<TagsTitleGetResponse>(x);
                  return item;

              });
            //var Fdata = data.Where(x => x.child_Tag != 0).Select(x =>
            //  {
            //      var item = _mapper.Map<TagsTitleGetDot>(x);
            //      return item;
            //  });
            //var items = data.Where(x => x.child_Tag == 0).Select(x =>
            //   {
            //       List<TagsTitleGetResponse> childrens = new List<TagsTitleGetResponse>();
            //       var child = Fdata.FirstOrDefault(r => r.child_Tag == x.Id);
            //       var item = _mapper.Map<TagsTitleGetResponse>(x);
            //       if (child != null)
            //       {
            //           var childs = _mapper.Map<TagsTitleGetResponse>(child);
            //           childrens.Add(childs);
            //           item.children = childrens;
            //       }
            //       return item;
            //   });

            //var result = new PagedResultDto<TagsTitleGetResponse>(items, count);
            //var obj = JsonConvert.SerializeObject(result);
            //_redis.StringSet("tags", obj);
            //_redis.KeyExpire("tags", DateTime.Now.AddHours(8));

            return new PagedResultDto<TagsTitleGetResponse>(items, count);
        }
    }


    public class TagsTitleGetRequest : IRequest<PagedResultDto<TagsTitleGetResponse>>
    {

    }

    public class TagsTitleGetResponse : EntityDto<int>
    {
        public string icon { get; set; }
        public string iconColor { get; set; }

        public string title { get; set; }
        /// <summary>
        /// 子标签
        /// </summary>
        //public List<TagsTitleGetResponse> children { get; set; }
        // public List<int> child_Tag { get; set; }
        public int? parent { get; set; }

    }

    public class TagsTitleGetDot : EntityDto<int>
    {
        public string icon { get; set; }
        public string iconColor { get; set; }

        public string title { get; set; }
        /// <summary>
        /// 子标签
        /// </summary>
        public List<TagsTitleGetResponse> children { get; set; }

        public int? child_Tag { get; set; }

    }
}
