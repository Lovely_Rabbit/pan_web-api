﻿using Microsoft.VisualBasic;
using Newtonsoft.Json;
using Pan.Common.Helper;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Pan.Domain.Caches
{
    public class RedisCache
    {
        private static readonly int DEFAULT_TMEOUT = 5000;// 默认超时时间（单位秒）
        private static readonly string coonstr = RedisConfig.RedisExchangeHosts;
        //private static readonly string coonstr = RedisConfig.RedisExchangeHostsdeputy;
        private static object _locker = new Object();
        private static ConnectionMultiplexer _instance = null;
        //public static Logger redisLogger { get; private set; }
        private static readonly string ID_PREFIX = Guid.NewGuid().ToString("N") + "-";

        private static Lazy<ConnectionMultiplexer> lazyConnection =
            new Lazy<ConnectionMultiplexer>(() => ConnectionMultiplexer.Connect(coonstr));
        /// <summary>
        /// 使用一个静态属性来返回已连接的实例，如下列中所示。这样，一旦 ConnectionMultiplexer 断开连接，便可以初始化新的连接实例。
        /// </summary>
        public static ConnectionMultiplexer Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_locker)
                    {
                        if (_instance == null || !_instance.IsConnected)
                        {
                            _instance = lazyConnection.Value;
                        }
                    }
                }

                //注册如下事件
                _instance.ConnectionFailed += MuxerConnectionFailed;// 连接失败 ， 如果重新连接成功你将不会收到这个通知
                _instance.ConnectionRestored += MuxerConnectionRestored;// 重新建立连接之前的错误
                _instance.ErrorMessage += MuxerErrorMessage;// 发生错误时
                _instance.ConfigurationChanged += MuxerConfigurationChanged;// 配置更改时
                _instance.HashSlotMoved += MuxerHashSlotMoved;// 更改集群
                _instance.InternalError += MuxerInternalError;// redis类库错误
                //_instance.PreserveAsyncOrder = false;
                //_instance.SyncTimeout = 5000;
                return _instance;
            }
        }

        public RedisCache()
        {
            //redisLogger = new Logger(AppDomain.CurrentDomain.BaseDirectory + "Logs", "redis_log_") { LogFileMaxLength = 200 };

            ThreadPool.SetMaxThreads(100, 100);

            ThreadPool.SetMinThreads(100, 100);
            //_db = GetDatabase(db);

        }


        /// <summary>
        /// 默认的 Key 值（用来当作 RedisKey 的前缀）
        /// </summary>
        private static readonly string DefaultKey = RedisConfig.RedisDefKey;

        //private static readonly string RedisDeltailKey = RedisConfig.RedisDeltailKey;

        /// <summary>
        /// 添加 Key 的前缀
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private static string AddKeyPrefix(string key)
        {
            // return DefaultKey + "" + key;
            return key;
        }

        /// <summary>
        /// 数据库
        /// </summary>
        //private IDatabase CachRedis => GetDatabase(db);
        //private readonly IDatabase _db => _instance.GetDatabase(db);
        //public RedisCache(int db = 0)
        //{
        //    _db = _instance.GetDatabase(db);
        //}

        public static IDatabase GetDatabase(int db)
        {
            return Instance.GetDatabase(db);
        }
        public ITransaction GetTransaction(int db)
        {
            return Instance.GetDatabase(db).CreateTransaction();
        }

        public static ISubscriber GetSubscriber()
        {
            return Instance.GetSubscriber();
        }
        public static void PubSub(string value, int db = 1)
        {
            //GetDatabase(db).Publish();
            //return Instance.GetSubscriber();
        }


        #region 获取

        public string Get(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).StringGet(key);
        }

        /// <summary>
        /// 获取 Key value 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static T Get<T>(string key, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                var cacheValue = GetDatabase(db).StringGet(key);
                var value = default(T);
                if (!cacheValue.IsNull)
                {
                    value = JsonConvert.DeserializeObject<T>(cacheValue);
                }
                return value;
            }
            catch (Exception)
            {
                return default(T);
            }
        }

        public static async Task<T> GetAsync<T>(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            var cacheValue = await GetDatabase(db).StringGetAsync(key);
            var value = default(T);
            if (!cacheValue.IsNull)
            {
                value = JsonConvert.DeserializeObject<T>(cacheValue);
            }
            return value;
        }

        public static string GetString(string key, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                var cacheValue = GetDatabase(db).StringGet(key);

                return cacheValue;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static T Get<T>(string key, Func<T> func, DateTime? cacheTime = null, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                if (Exists(key, db))
                {
                    return Get<T>(key, db);
                }
                else
                {
                    cacheTime ??= DateTime.Now.AddDays(1);
                    var obj = func.Invoke();
                    Set(key, obj, cacheTime.Value, db);
                    return obj;
                }
            }
            catch (Exception)
            {
                return func.Invoke();
            }
        }

        public static string GetStrings(string key, Func<string> func, DateTime? cacheTime = null, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                if (Exists(key, db))
                {
                    return GetString(key, db);
                }
                else
                {
                    cacheTime ??= DateTime.Now.AddDays(1);
                    var obj = func.Invoke();
                    Set(key, obj, cacheTime.Value, db);
                    return obj;
                }
            }
            catch (Exception)
            {
                return func.Invoke();
            }
        }

        /// <summary>
        /// 第一次查询数据库 如果数据库为没有查询到 返回一个null 的对象
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="func"></param>
        /// <param name="cacheTime"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static async Task<T> GetOrDbAsync<T>(string key, Func<Task<T>> func, DateTime? cacheTime = null, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                Console.WriteLine("当前key:", key);
                if (await ExistsAsync(key, db))
                {
                    Console.WriteLine("访问了==>缓存=>");
                    return await GetAsync<T>(key, db);
                }
                else
                {
                    cacheTime ??= DateTime.Now.AddDays(1);
                    var obj = await func.Invoke();
                    Console.WriteLine("访问了==>数据库=>");
                    if (obj == null)
                    {
                        return obj;
                    }
                    await SetAsync(key, obj, cacheTime.Value, db);
                    return obj;
                }
            }
            catch (Exception)
            {
                return await func.Invoke();
            }
        }

        public static async Task<T> GetOrDb<T>(string key, Func<T> func, DateTime? cacheTime = null, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                Console.WriteLine("当前key:", key);
                if (await ExistsAsync(key, db))
                {
                    Console.WriteLine("访问了==>缓存=>");
                    return await GetAsync<T>(key, db);
                }
                else
                {
                    cacheTime ??= DateTime.Now.AddDays(1);
                    var obj = func.Invoke();
                    Console.WriteLine("访问了==>数据库=>");
                    if (obj == null)
                    {
                        return obj;
                    }
                    await SetAsync(key, obj, cacheTime.Value, db);
                    return obj;
                }
            }
            catch (Exception)
            {
                return func.Invoke();
            }
        }


        public static string GetString(string key, Func<string> func, DateTime? cacheTime = null, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                if (Exists(key, db))
                {
                    return GetString(key, db);
                }
                else
                {
                    cacheTime ??= DateTime.Now.AddDays(1);
                    var obj = func.Invoke();
                    Set(key, obj, cacheTime.Value, db);
                    return obj;
                }
            }
            catch (Exception)
            {
                return func.Invoke();
            }
        }

        #endregion 获取



        #region 异步获取

        public static async Task<string> GetAsync(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await GetDatabase(db).StringGetAsync(key);
        }

        public static async Task<long> HashDeleteAsync(string key, List<RedisValue> dataKeys, int db = 4)
        {
            try
            {
                return await GetDatabase(db).HashDeleteAsync(key, dataKeys.ToArray());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                throw;
            }

        }

        #endregion 异步获取

        #region 设置

        /// <summary>
        /// 设置缓存，字符串类型
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="data"> 缓存Value 字符串 </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> 结果 </returns>
        public static bool Set(string key, string data, int db = 0)
        {
            key = AddKeyPrefix(key);
            return Set(key, data, DEFAULT_TMEOUT, db);
        }

        /// <summary>
        /// 设置缓存，字符串类型
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="data"> 缓存Value 字符串 </param>
        /// <param name="seconds"> seconds秒后，清除缓存 </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> 结果 </returns>
        public static bool Set(string key, string data, int seconds, int db = 0)
        {
            key = AddKeyPrefix(key);
            return Set(key, data, DateTime.Now.AddSeconds(seconds), db);
        }

        /// <summary>
        /// 永不过期
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static bool SetToken<T>(string key, T data, int db = 0)
        {
            return GetDatabase(db).StringSet(key, JsonConvert.SerializeObject(data));
        }

        /// <summary>
        /// 设置缓存，字符串类型
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="data"> 缓存Value 字符串 </param>
        /// <param name="cacheTime"> 指定时间，清除缓存 </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> 结果 </returns>
        public static bool Set(string key, string data, DateTime cacheTime, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).StringSet(key, data, (cacheTime - DateTime.Now));
        }

        /// <summary>
        /// 设置缓存，T类型 转字符串json格式
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="data"> 缓存Value 字符串 </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> 结果 </returns>
        public static bool Set<T>(string key, T data, int db = 0)
        {
            key = AddKeyPrefix(key);
            return Set<T>(key, data, DEFAULT_TMEOUT, db);
        }

        /// <summary>
        /// 设置缓存，T类型 转字符串json格式
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="data"> 缓存Value 字符串 </param>
        /// <param name="seconds"> seconds秒后，清除缓存 </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> 结果 </returns>
        public static bool Set<T>(string key, T data, int seconds, int db = 0)
        {
            key = AddKeyPrefix(key);
            return Set<T>(key, data, DateTime.Now.AddSeconds(seconds), db);
        }

        /// <summary>
        /// 设置缓存，T类型 转字符串json格式
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="data"> 缓存Value 字符串 </param>
        /// <param name="cacheTime"> 指定时间，清除缓存 </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> 结果 </returns>
        public static bool Set<T>(string key, T data, DateTime cacheTime, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                TimeSpan timeSpan = cacheTime - DateTime.Now;
                return GetDatabase(db).StringSet(key, JsonConvert.SerializeObject(data), timeSpan);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion 设置

        #region 异步设置

        public static async Task<bool> SetAsync(string key, string data, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await SetAsync(key, data, DEFAULT_TMEOUT, db);
        }

        public static async Task<bool> SetAsync(string key, string data, int seconds, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await SetAsync(key, data, DateTime.Now.AddSeconds(seconds), db);
        }

        public static async Task<bool> SetAsync(string key, string data, DateTime cacheTime, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await GetDatabase(db).StringSetAsync(key, data, (cacheTime - DateTime.Now));
        }

        public static async Task<bool> SetAsync<T>(string key, T data, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await SetAsync<T>(key, data, DEFAULT_TMEOUT, db);
        }

        public static async Task<bool> SetAsync<T>(string key, T data, int seconds, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await SetAsync<T>(key, data, DateTime.Now.AddSeconds(seconds), db);
        }

        public static async Task<bool> SetAsync<T>(string key, T data, DateTime cacheTime, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await GetDatabase(db).StringSetAsync(key, JsonConvert.SerializeObject(data), (cacheTime - DateTime.Now));
        }

        #endregion 异步设置

        #region 列表

        public static List<T> GetList<T>(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            var list = GetDatabase(db).ListRange(key);
            List<T> result = new List<T>();
            foreach (var item in list)
            {
                result.Add(JsonConvert.DeserializeObject<T>(item));
            }
            return result;
        }

        /// <summary>
        /// 从底部插入数据
        /// </summary>
        public static void ListRightPush<T>(string key, List<T> data, int db = 0)
        {
            key = AddKeyPrefix(key);
            foreach (var single in data)
            {
                GetDatabase(db).ListRightPush(key, JsonConvert.SerializeObject(single));
            }
        }

        /// <summary>
        /// 从顶部插入数据
        /// </summary>
        public static void ListLeftPush<T>(string key, List<T> data, int db = 0)
        {
            key = AddKeyPrefix(key);
            foreach (var single in data)
            {
                GetDatabase(db).ListLeftPush(key, JsonConvert.SerializeObject(single));
            }
        }

        /// <summary>
        /// 从底部拿出数据，并删除此行数据
        /// </summary>
        public static T ListRightPop<T>(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            var rightPop = GetDatabase(db).ListRightPop(key);
            return rightPop.HasValue ? JsonConvert.DeserializeObject<T>(rightPop) : default(T);
        }

        /// <summary>
        /// 从顶部拿出数据，并删除此行数据
        /// </summary>
        public static T ListLeftPop<T>(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            RedisValue leftpop = GetDatabase(db).ListLeftPop(key);
            return leftpop.HasValue ? JsonConvert.DeserializeObject<T>(leftpop) : default(T);
        }

        #endregion 列表

        /// <summary>
        /// 点赞自增
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="count"> </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static long PointIncrement1(string key, int count, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).StringIncrement(key, count);
        }

        #region 简单 redis 队列

        /// <summary>
        /// 存，从顶部插入数据，简单 redis 队列
        /// </summary>
        public static long LeftPush(string key, string data, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).ListLeftPush(key, data);
        }

        /// <summary>
        /// 存，从顶部插入数据，简单 redis 队列
        /// </summary>
        public static long LeftPushByList(string key, List<string> list, int db = 0)
        {
            key = AddKeyPrefix(key);
            if (list != null && list.Count > 0)
            {
                return GetDatabase(db).ListLeftPush(key, list.Select(o => (RedisValue)o).ToArray());
            }
            return 0;
        }

        /// <summary>
        /// 取，从底部拿出数据，并删除此行数据，简单 redis 队列
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static string RightPop(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).ListRightPop(key);
        }

        #endregion 简单 redis 队列

        #region Hash操作
        /// <summary>
        /// 异步可批量设置Hash(字典)
        /// </summary>
        /// <param name="key"></param>
        /// <param name="entries"></param>
        /// <returns></returns>
        public static async Task HashSetAsync(RedisKey key, HashEntry[] entries, int db = 0)
        {

            key = AddKeyPrefix(key);
            await GetDatabase(db).HashSetAsync(key, entries);
        }




        /// <summary>
        /// 存储数据到Hash 表
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="dataKey"></param>
        /// <param name="t"></param>
        /// <param name="db"></param>
        /// <returns></returns>
        public static async Task<bool> HashSetAsync<T>(string key, string dataKey, T t, int db = 0)
        {
            key = AddKeyPrefix(key);
            string json = ConvertJson(t);
            return await GetDatabase(db).HashSetAsync(key, dataKey, json);
        }

        /// <summary>
        ///  异步根据键获取值
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static async Task<RedisValue[]> HashValuesAsync(RedisKey key, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await GetDatabase(db).HashValuesAsync(key);
        }


        public static IAsyncEnumerable<HashEntry> HashScanAsync(RedisKey key, int db = 0)
        {
            key = AddKeyPrefix(key);
          
            return GetDatabase(db).HashScanAsync(key, "*",50);
        }

        /// <summary>
        /// 异步根据键获取键值对
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static async Task<HashEntry[]> HashGetAllAsync(RedisKey key, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await GetDatabase(db).HashGetAllAsync(key);
        }

        /// <summary>
        /// 根据键和和键值对的键获取某个对应的值
        /// </summary>
        /// <param name="key"></param>
        /// <param name="field"></param>
        /// <returns></returns>
        public static async Task<RedisValue> HashGetAsync(RedisKey key, RedisValue field, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await GetDatabase(db).HashGetAsync(key, field);
        }
        #endregion

        #region 其他

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> </returns>

        public static bool Remove(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).KeyDelete(key, CommandFlags.HighPriority);
        }

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> </returns>
        public static async Task<bool> RemoveAsync(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await GetDatabase(db).KeyDeleteAsync(key, CommandFlags.HighPriority);
        }

        /// <summary>
        /// 是否存在
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> </returns>
        public static bool Exists(string key, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                return GetDatabase(db).KeyExists(key);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 是否存在异步版本
        /// </summary>
        /// <param name="key"> 缓存Key </param>
        /// <param name="db"> 数据库编号 </param>
        /// <returns> </returns>
        public static async Task<bool> ExistsAsync(string key, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                return await GetDatabase(db).KeyExistsAsync(key);
            }
            catch (Exception)
            {
                return false;
            }
        }

        /// <summary>
        /// 实现递增
        /// </summary>
        public static async Task<long> Increment(string key, long value, int db = 0)
        {
            key = AddKeyPrefix(key);
            // 三种命令模式 Sync,同步模式会直接阻塞调用者，但是显然不会阻塞其他线程。 Async,异步模式直接走的是Task模型。 Fire - and -
            // Forget,就是发送命令，然后完全不关心最终什么时候完成命令操作。 即发即弃：通过配置 CommandFlags
            // 来实现即发即弃功能，在该实例中该方法会立即返回，如果是string则返回null 如果是int则返回0.这个操作将会继续在后台运行，一个典型的用法页面计数器的实现：
            return await GetDatabase(db).StringIncrementAsync(key, value, CommandFlags.None);
        }

        /// <summary>
        /// 实现递减
        /// </summary>
        public static long Decrement(string key, string datakey, long value, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).HashDecrement(key, datakey, value, flags: CommandFlags.FireAndForget);
        }

        /// <summary>
        /// 发送信息
        /// </summary>
        public static long Publish(string channel, object message)
        {

            return GetSubscriber().Publish(channel, JsonConvert.SerializeObject(message));
        }

        public static long Publish<T>(string channel, T msg)
        {
            return GetSubscriber().Publish(channel, ConvertJson(msg));
        }

        /// <summary>
        /// 在消费者端得到该消息并输出
        /// </summary>
        public static void Subscribe(string channelFrom)
        {
            GetSubscriber().Subscribe(channelFrom, (channel, message) =>
            {
                Console.WriteLine(message);
            });
        }

        /// <summary>
        /// 消费消息回调
        /// </summary>
        /// <param name="subChannel"></param>
        /// <param name="handler"></param>
        public static void SubscribeBlack(string subChannel, Action<RedisChannel, RedisValue> handler = null)
        {
            //ISubscriber sub = _conn.GetSubscriber();
            GetSubscriber().Subscribe(subChannel, (channel, message) =>
            {
                if (handler == null)
                {
                    Console.WriteLine(subChannel + " 订阅收到消息：" + message);
                }
                else
                {
                    handler(channel, message);
                }
            });
        }

        /// <summary>
        /// GetServer方法会接收一个EndPoint类或者一个唯一标识一台服务器的键值对 有时候需要为单个服务器指定特定的命令
        /// 使用IServer可以使用所有的shell命令，比如： DateTime lastSave = server.LastSave(); ClientInfo[] clients
        /// = server.ClientList(); 如果报错在连接字符串后加 ,allowAdmin=true;
        /// </summary>
        public static IServer GetServer(string host, int port)
        {
            IServer server = Instance.GetServer(host, port);
            return server;
        }

        /// <summary>
        /// 获取全部终结点
        /// </summary>
        public static EndPoint[] GetEndPoints()
        {
            EndPoint[] endpoints = Instance.GetEndPoints();
            return endpoints;
        }

        #endregion 其他

        #region 事件

        /// <summary>
        /// 配置更改时
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        private static void MuxerConfigurationChanged(object sender, EndPointEventArgs e)
        {
            // LogHelper.WriteInfoLog("Configuration changed: " + e.EndPoint);
        }

        /// <summary>
        /// 发生错误时
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        private static void MuxerErrorMessage(object sender, RedisErrorEventArgs e)
        {
            // LogHelper.WriteInfoLog("ErrorMessage: " + e.Message);
        }

        /// <summary>
        /// 重新建立连接之前的错误
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        private static void MuxerConnectionRestored(object sender, ConnectionFailedEventArgs e)
        {
            // LogHelper.WriteInfoLog("ConnectionRestored: " + e.EndPoint);
        }

        /// <summary>
        /// 连接失败 ， 如果重新连接成功你将不会收到这个通知
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        private static void MuxerConnectionFailed(object sender, ConnectionFailedEventArgs e)
        {
            // LogHelper.WriteInfoLog("重新连接：Endpoint failed: " + e.EndPoint + ", " + e.FailureType +
            // (e.Exception == null ? "" : (", " + e.Exception.Message)));
        }

        /// <summary>
        /// 更改集群
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        private static void MuxerHashSlotMoved(object sender, HashSlotMovedEventArgs e)
        {
            // LogHelper.WriteInfoLog("HashSlotMoved:NewEndPoint" + e.NewEndPoint + ", OldEndPoint"
            // + e.OldEndPoint);
        }

        /// <summary>
        /// redis类库错误
        /// </summary>
        /// <param name="sender"> </param>
        /// <param name="e"> </param>
        private static void MuxerInternalError(object sender, InternalErrorEventArgs e)
        {
            // LogHelper.WriteInfoLog("InternalError:Message" + e.Exception.Message);
        }

        #endregion 事件

        #region ZSet

        /// <summary>
        /// 操作ZSet
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="data"> </param>
        /// <param name="score"> 权重值 </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static bool SortedSetAdd(string key, string data, double score, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).SortedSetAdd(key, data, score);
        }

        /// <summary>
        /// 操作ZSet 递增
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="data"> </param>
        /// <param name="score"> 权重值 </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static async Task<double> SortedSetIncrementAsync(string key, string data, double score, int db = 0)
        {
            key = AddKeyPrefix(key);
            return await GetDatabase(db).SortedSetIncrementAsync(key, data, score, CommandFlags.None);
        }

        /// <summary>
        /// 查询ZSet 返回排名 与 分数
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="order"> 排序方式 </param>
        /// <param name="start"> 查找开始位置，从索引0开始 </param>
        /// <param name="stop"> 查找个数 </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static async Task<long> SortedSetRangeByRankWithScores(RedisKey key, SortedSetEntry[] values, CommandFlags flags, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                var context = GetDatabase(db);
                var vs = await context.SortedSetAddAsync(key, values, CommandFlags.None);
                return vs;
            }
            catch (Exception)
            {
                return 0;
            }
        }

        /// <summary>
        /// 查询ZSet 返回排名 与 分数
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="order"> 排序方式 </param>
        /// <param name="start"> 查找开始位置，从索引0开始 </param>
        /// <param name="stop"> 查找个数 </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static async Task<List<RespZSet>> SortedSetRangeByRankWithScores(string key, Func<List<RespZSet>> fun, int start = 0, int stop = -1, int order = 1, int db = 0)
        {
            try
            {
                key = AddKeyPrefix(key);
                var context = GetDatabase(db);

                SortedSetEntry[] vs = await context.SortedSetRangeByRankWithScoresAsync(key, start, stop, (Order)order, CommandFlags.None);

                List<RespZSet> list = new List<RespZSet>();
                int i = 0;
                foreach (var item in vs)
                {
                    list.Add(new RespZSet()
                    {
                        Key = key,
                        Data = item.Element.ToString(),
                        Score = item.Score,
                        Ranking = start + 1 + i
                    });
                    i++;
                }
                return list;
            }
            catch (Exception)
            {
                return fun.Invoke();
            }
        }

        /// <summary>
        /// 获取有序有序列表所有数据值
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static async Task<long> SortedSetLengthAsync(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            var result = await GetDatabase(db).SortedSetLengthAsync(key);
            return result;
        }

        /// <summary>
        /// 根据key 与值获取排名
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="data"> </param>
        /// <param name="order"> 0 正序 1倒叙 </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static async Task<long> SortedSetRankAsync(string key, string data, int order, int db = 0)
        {
            key = AddKeyPrefix(key);
            var result = await GetDatabase(db).SortedSetRankAsync(key, data, (Order)order, CommandFlags.None);
            return result == null ? 0 : result.Value + 1;
        }

        /// <summary>
        /// 根据key 与值获取积分
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="data"> </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static async Task<long> SortedSetScoreAsync(string key, string data, int db = 0)
        {
            key = AddKeyPrefix(key);
            var result = await GetDatabase(db).SortedSetScoreAsync(key, data, CommandFlags.None);
            return result == null ? 0 : (long)result.Value;
        }

        #endregion ZSet

        #region 锁  LockTake

        /// <summary>
        /// 分布式锁
        /// </summary>
        public static bool LockTake(string key, string datakey, int db = 0)
        {
            while (true)
            {
                key = AddKeyPrefix(key);
                var flag = GetDatabase(db).LockTake(key, datakey, TimeSpan.FromSeconds(10.0), CommandFlags.None);
                if (flag)
                {//如果加锁成功
                    return true;
                }
                Thread.Sleep(200);
            }
        }

        /// <summary>
        /// 分布式锁封装（委托）
        /// </summary>
        /// <param name="key"> </param>
        /// <param name="action"> </param>
        /// <param name="db"> </param>
        /// <returns> </returns>
        public static async Task<T> LockTakeFunc<T>(string key, Func<Task<T>> action, int db = 0)
        {
            key = AddKeyPrefix(key);
            string lockval = Guid.NewGuid().ToString();

            var lockobj = LockTake(key, lockval, db);
            if (lockobj)//如果加锁成功
            {
                //Action act = () =>
                //{
                //    int i = 0;
                //    while (actBool)
                //    {
                //        if (i > 10) break;//超过10次，无论如何也停下来吧
                //        Thread.Sleep(3000);
                //        if (actBool == false) break;//优化，刚等完3秒如果需要退出进程，立即退出，不再更新过期时间
                //        Set(key, lockval, DateTime.Now.ToARCBjTime().AddSeconds(10), db);
                //        i++;
                //    }
                //};
                //act.BeginInvoke(null, null);
                var result = default(T);
                try
                {
                    result = await action.Invoke();
                }
                catch (Exception e)
                {
                    // LogHelper.Error(JsonConvert.SerializeObject(e));
                }
                finally
                {
                    GetDatabase(db).LockRelease(key, key);
                    string value = await GetAsync(key, db);
                    if (value == key)
                    {
                        Remove(key, db);
                    }
                }

                return result;
            }
            return default(T);
        }

        public static bool LockRelease(string key, string datakey, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).LockRelease(key, datakey);
        }


        #endregion 锁  LockTake

        #region 分布式锁
        public static bool LockByRedis(string key, double expireTimeSeconds = 600, int db = 0)
        {
            try
            {
                while (true)
                {
                    //Console.WriteLine($"生成的GUID ：{ID_PREFIX}");
                    //expireTimeSeconds = expireTimeSeconds > 20 ? 10 : expireTimeSeconds;
                    bool lockflag = GetDatabase(db).LockTake(key, ID_PREFIX + Thread.CurrentThread.ManagedThreadId,
                        TimeSpan.FromSeconds(expireTimeSeconds));
                    if (lockflag) //如果锁定成功，则为 true，否则为 false，否则为 false。 
                    {
                        //break;
                        return true;

                    }
                    return false;
                }
                //return database().LockTake(key, ID_PREFIX + Thread.CurrentThread.ManagedThreadId, TimeSpan.FromSeconds(expireTimeSeconds));
            }
            catch (Exception ex)
            {
                throw new Exception($"Redis加锁异常:原因{ex.Message}");
            }
        }

        /// <summary>
        /// 释放锁
        /// </summary>
        /// <param name="name"></param>
        public static void UnLockByRedis(string key, int db = 0)
        {
            try
            {
                //判读线程标识是否一致 锁是否一致
                //1、查询redis 锁
                string threadid = ID_PREFIX + Thread.CurrentThread.ManagedThreadId;
                string id = GetDatabase(db).StringGet(key).ToString();
                if (threadid.Equals(id)) //防止锁被误删
                    GetDatabase(db).LockRelease(key, ID_PREFIX + Thread.CurrentThread.ManagedThreadId);
            }
            catch (Exception ex)
            {
                throw new Exception($"Redis加锁异常:原因{ex.Message}");
            }
        }
        #endregion

        #region Set

        /// <summary>
        /// 在Key集合中添加一个value值
        /// </summary>
        /// <typeparam name="T"> 数据类型 </typeparam>
        /// <param name="key"> Key名称 </param>
        /// <param name="value"> 值 </param>
        /// <returns> </returns>
        public static bool SetAdd<T>(string key, T value, int db = 0)
        {
            key = AddKeyPrefix(key);
            string jValue = ConvertJson(value);
            return GetDatabase(db).SetAdd(key, jValue);
        }

        private static string ConvertJson<T>(T value)
        {
            //string result = value is string ? value.ToString() : JsonConvert.SerializeObject(value);
            //return result;
            string result = value is string ? value.ToString() :
                JsonConvert.SerializeObject(value, Formatting.None);
            return result;
        }

        private static RedisValue[] ConvertRedisValue<T>(params T[] redisValues) => redisValues.Select(o => (RedisValue)ConvertJson<T>(o)).ToArray();

        /// <summary>
        /// 将值反系列化成对象
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="value"> </param>
        /// <returns> </returns>
        private static T ConvertObj<T>(RedisValue value)
        {
            return value.IsNullOrEmpty ? default(T) : JsonConvert.DeserializeObject<T>(value);
        }

        /// <summary>
        /// 将值反系列化成对象集合
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="values"> </param>
        /// <returns> </returns>
		private static List<T> ConvetList<T>(RedisValue[] values)
        {
            List<T> result = new List<T>();
            foreach (var item in values)
            {
                var model = ConvertObj<T>(item);
                result.Add(model);
            }
            return result;
        }

        /// <summary>
        /// 在Key集合中添加多个value值
        /// </summary>
        /// <typeparam name="T"> 数据类型 </typeparam>
        /// <param name="key"> Key名称 </param>
        /// <param name="value"> 值列表 </param>
        /// <returns> </returns>
        public static long SetAdd<T>(string key, List<T> value, int db = 0)
        {
            key = AddKeyPrefix(key);
            RedisValue[] valueList = ConvertRedisValue(value.ToArray());
            return GetDatabase(db).SetAdd(key, valueList);
        }

        /// <summary>
        /// 获取key集合值的数量
        /// </summary>
        /// <param name="key"> </param>
        /// <returns> </returns>
		public static long SetLength(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            return GetDatabase(db).SetLength(key);
        }

        /// <summary>
        /// 判断Key集合中是否包含指定的值
        /// </summary>
        /// <typeparam name="T"> 值类型 </typeparam>
        /// <param name="key"> </param>
        /// <param name="value"> 要判断是值 </param>
        /// <returns> </returns>
        public static bool SetContains<T>(string key, T value, int db = 0)
        {
            key = AddKeyPrefix(key);
            string jValue = ConvertJson(value);
            return GetDatabase(db).SetContains(key, jValue);
        }

        /// <summary>
        /// 随机获取key集合中的一个值
        /// </summary>
        /// <typeparam name="T"> 数据类型 </typeparam>
        /// <param name="key"> </param>
        /// <returns> </returns>
        public static T SetRandomMember<T>(string key, int db = 0)
        {
            key = AddKeyPrefix(key);
            var rValue = GetDatabase(db).SetRandomMember(key);
            return ConvertObj<T>(rValue);
        }

        /// <summary>
        /// 获取key所有值的集合
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="key"> </param>
        /// <returns> </returns>
        public static List<T> SetMembers<T>(string key, int db = 0)
        {
            var rValue = GetDatabase(db).SetMembers(key);
            return ConvetList<T>(rValue);
        }

        /// <summary>
        /// 删除key集合中指定的value
        /// </summary>
        /// <typeparam name="T"> </typeparam>
        /// <param name="key"> </param>
        /// <param name="value"> </param>
        /// <returns> </returns>
        public static long SetRemove<T>(string key, int db = 0, params T[] value)
        {
            key = AddKeyPrefix(key);
            RedisValue[] valueList = ConvertRedisValue(value);
            return GetDatabase(db).SetRemove(key, valueList);
        }

        #endregion Set

        public static HashEntry[] ConvertObjectToHashEntry(object obj)
        {
            PropertyInfo[] properties = obj.GetType().GetProperties();

            List<HashEntry> hashEntries = new List<HashEntry>();

            foreach (PropertyInfo property in properties)
            {
                string fieldName = property.Name;
                object value = property.GetValue(obj);

                byte[] fieldValue = SerializeObject(value);

                hashEntries.Add(new HashEntry(fieldName, fieldValue));
            }

            return hashEntries.ToArray();
        }

        private static byte[] SerializeObject(object obj)
        {
            if (obj == null) return null;
            string json = JsonConvert.SerializeObject(obj);
            return Encoding.UTF8.GetBytes(json);
        }


    }

    public class RespZSet
    {
        public string Key { get; set; }
        public string Data { get; set; }
        public double Score { get; set; }
        public long Ranking { get; set; }
    }

    public static class RedisConfig
    {
        static RedisConfig()
        {
            ConfigHelper.BindStatic(typeof(RedisConfig), false, "RedisConfig");
        }

        public static string RedisExchangeHosts { get; set; }
        public static string RedisDefKey { get; set; } //默认 post 列表key
        /// <summary>
        /// 默认连接字符串
        /// </summary>
        public static string RedisExchangeHostsdeputy { get; set; }
        /// <summary>
        /// 默认详情文章连接字符串
        /// </summary>
        public static string RedisDeltailKey { get; set; }

        public static string IpLog { get; set; }
    }
}
