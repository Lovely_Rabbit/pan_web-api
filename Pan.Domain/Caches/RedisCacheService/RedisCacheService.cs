﻿using Newtonsoft.Json;
using System;
using System.Linq;
using System.Text;

namespace Pan.Domain.Caches.RedisCacheService
{
    public class RedisCacheService : ICacheService
    {
        private readonly string _instance;
        private readonly JsonSerializerSettings _jsonSerializerSettings;
        public RedisCacheService(string instance)
        {
            _instance = instance;
            _jsonSerializerSettings = new JsonSerializerSettings();
            _jsonSerializerSettings.StringEscapeHandling = StringEscapeHandling.EscapeNonAscii;
        }

        /// <summary>
        /// 获取key名称
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private string GetKeyForRedis(string key)
        {
            return _instance + key;
        }

        /// <summary>
        /// 序列化json
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string SerializeObj(object value)
        {
            return JsonConvert.SerializeObject(value, _jsonSerializerSettings);
        }

        /// <summary>
        /// 验证缓存是否存在
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Exists(string key)
        {
            return RedisHelper.Exists(GetKeyForRedis(key));
        }

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public object Get(string key)
        {
            return JsonConvert.DeserializeObject(RedisHelper.Get(GetKeyForRedis(key)));
        }

        /// <summary>
        /// 获取缓存
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T Get<T>(string key) where T : class
        {
            return JsonConvert.DeserializeObject<T>(RedisHelper.Get(GetKeyForRedis(key)));
        }

        /// <summary>
        /// 添加缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiresIn"></param>
        /// <returns></returns>
        public bool Add(string key, object value, TimeSpan expiresIn)
        {
            return RedisHelper.Set(GetKeyForRedis(key), Encoding.UTF8.GetBytes(SerializeObj(value)), (int)expiresIn.TotalSeconds);
        }

        /// <summary>
        /// 删除缓存
        /// </summary>
        /// <param name="key"></param>
        public void Remove(params string[] key)
        {
            RedisHelper.Del(key.Select(i => GetKeyForRedis(i)).ToArray());
        }

        /// <summary>
        /// 修改缓存
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="expiresIn"></param>
        /// <returns></returns>
        public bool Replace(string key, object value, TimeSpan expiresIn)
        {
            if (Exists(key))
            {
                Remove(key);
            }
            return Add(key, value, expiresIn);
        }

        /// <summary>
        /// 自增 1
        /// </summary>
        /// <param name="key"></param>
        public bool IncrBy(string key)
        {
            var isIncr = RedisHelper.IncrBy(key);
            if (isIncr == 0)
            {
                return false;
            }
            return true;
        }
    }
}
