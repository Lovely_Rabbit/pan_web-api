﻿using Microsoft.Extensions.Caching.Memory;
using MongoDB.Bson;
using MongoDB.Driver;
using Nest;
using Pan.Domain.Handler.Post;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.EnumExceptions;
using Pan.Infrastructure.Enums;
using Pan.Infrastructure.MongdbContext;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using static RabbitMQ.Client.Logging.RabbitMqClientEventSource;

namespace Pan.Domain.Caches.PostCacheService
{
    public class PostAllMdbCacheService : CacheBase<PagedResultDto<PostItems>, string>
    {
        private readonly string Prefix = RedisCachePrefixEnums.Post.GetDescriptionByEnum();
        private readonly IMongoRepository<PostItems> _mongoRepository;
        public string Keyword = string.Empty;

        public PostAllMdbCacheService(IMemoryCache cache, IMongoRepository<PostItems> mongoRepository) : base(cache)
        {
            _mongoRepository = mongoRepository;
            cacheName = cacheKey;
            var request = new PagedAndSortedRequest() { };
            if (request.SkipCount == 0) request.SkipCount = 1;
            expiryTime = TimeSpan.FromHours(1);
            func = async () =>
            {
                FilterDefinition<PostItems> filter = Builders<PostItems>.Filter.Empty;
                Keyword = cacheKey.Replace(Prefix, "");
                if (Keyword != "0")
                {
                    filter = Builders<PostItems>.Filter.And(
              Builders<PostItems>.Filter.Regex(u => u.Title,
              new BsonRegularExpression(Regex.Escape(Keyword), "i")));
                }
                //Builders<PostItems>.Filter.Regex(u => u.htmlContext,
                // new BsonRegularExpression(Regex.Escape(request.Title), "i"))
                string[] fields = { "Title", "id", "tags_title", "CreateOn", "HashV" };
                var sortDeftion = Builders<PostItems>.Sort.Descending(x => x.CreateOn);

                //var items = await _mongoRepository.FindListByPageAsync(filter, request.SkipCount
                //    , request.MaxResultCount, fields, sortDeftion)
                //    ;
                FilterDefinition<PostItems> filtercount = Builders<PostItems>.Filter.Empty;
                long total = await _mongoRepository.CountAsync(filtercount);
                List<PostItems> items = await _mongoRepository.FindListByPageAsync(filter, request.SkipCount
                 , request.MaxResultCount, fields, sortDeftion)
                 ; ;
                await RedisCache.GetOrDb(cacheKey, () =>
                {
                    return new PagedResultDto<PostItems>(items, total);
                }, DateTime.Now.AddDays(1), 0);

                return new PagedResultDto<PostItems>(items, total);
            };
        }



    }
}
