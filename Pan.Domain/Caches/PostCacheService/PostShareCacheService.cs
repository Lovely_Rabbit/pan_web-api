﻿using Mapster.Utils;
using MediatR;
using Microsoft.Extensions.Caching.Memory;
using Pan.Domain.Handler.Post;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http.Headers;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using static RabbitMQ.Client.Logging.RabbitMqClientEventSource;

namespace Pan.Domain.Caches.PostCacheService
{
    public class PostShareCacheService : CacheBase<HttpStatusCode, string>
    {
        public PostShareCacheService(IMemoryCache cache) : base(cache)
        {
            cacheName = "";
            expiryTime = TimeSpan.FromDays(1);
            func = async () =>
            {
                using (HttpClient httpClient = new HttpClient())
                {
                    httpClient.Timeout = TimeSpan.FromSeconds(10);
                    string paly = "{\"share_id\":\"" + cacheKey + "\"}";
                    using var httcontent = new StringContent(paly);
                    httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    var rep = await httpClient.PostAsync($"https://api.aliyundrive.com/adrive/v3/share_link/get_share_by_anonymous?share_id={cacheKey}"
                        , httcontent);
                    string reponseBody = await rep.Content.ReadAsStringAsync();
                    // return Json(new ResponseValue<HttpStatusCode>(rep.StatusCode));// rep.StatusCode;
                    await RedisCache.GetOrDb(cacheName + cacheKey, () =>
                    {
                        return rep.StatusCode;
                    }, DateTime.Now.AddYears(1), 2);
                    return rep.StatusCode;
                }
            };
        }
    }
}
