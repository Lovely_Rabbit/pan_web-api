﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using MongoDB.Bson;
using MongoDB.Driver;
using Pan.Domain.Handler.Post;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.EnumExceptions;
using Pan.Infrastructure.Enums;
using Pan.Infrastructure.MongdbContext;
using System;
using System.Linq;
using static RabbitMQ.Client.Logging.RabbitMqClientEventSource;
using System.Text.RegularExpressions;
using MongoDB.Bson.Serialization.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace Pan.Domain.Caches.PostCacheService
{

    public class PostItemResponse //: post_html
    {
        public string HashV { get; set; }
        public string htmlContext { get; set; }
    }
    [Table("post")]
    public class PostItemsReponse //: PostItems
    {
        [BsonId]
        public int id { get; set; }
        [BsonElement("htmlContext")]
        public string htmlContext { get; set; }

        [BsonElement("HashV")]
        public string HashV { get; set; }
    }


    public class PostMdbCacheService : CacheBase<PostsGetResponse, string>
    {
        private readonly string Prefix = RedisCachePrefixEnums.Post.GetDescriptionByEnum();
        private readonly IMongoRepository<PostItemsReponse> _mongoRepository;
        public PostMdbCacheService(IMemoryCache cache, IMongoRepository<PostItemsReponse> mongoRepository) : base(cache)
        {
            _mongoRepository = mongoRepository;

            cacheName = cacheKey;
            cacheName = Prefix;
            expiryTime = TimeSpan.FromHours(1);
            func = async () =>
            {
                FilterDefinition<PostItemsReponse> filter = Builders<PostItemsReponse>.Filter
                    .And(
                    Builders<PostItemsReponse>.Filter.Eq(x => x.HashV, cacheKey)
                    );
                var fields = new string[] { "HashV", "htmlContext" };
                var item = await _mongoRepository.GetByHasvAsync(filter, fields);
                if (item is null)
                    throw new ArgumentNullException("未找到当前信息！");
                return new PostsGetResponse
                {
                    HashV = item.HashV,
                    htmlContext = item.htmlContext.Split(","),
                };
            };
        }
    }


    public class PostCacheService : CacheBase<PostsGetResponse, string>
    {

        private readonly string Prefix = RedisCachePrefixEnums.Post.GetDescriptionByEnum();
        private readonly IRepository<post_html> _htmlRepository;

        public PostCacheService(IMemoryCache cache, IRepository<post_html> threadRepository) : base(cache)
        {
            _htmlRepository = threadRepository;
            cacheName = Prefix;
            expiryTime = TimeSpan.FromHours(1);
            func = async () =>
            {
                var item = await _htmlRepository.GetQueryWithDisable()
               .Where(x => x.HashV == cacheKey)
               .FirstOrDefaultAsync();
                if (item is null)
                    throw new ArgumentNullException("未找到当前信息！");
                return new PostsGetResponse
                {
                    Id = item.Id,
                    htmlContext = item.html_context.Split(","),
                };
            };
        }
    }
}
