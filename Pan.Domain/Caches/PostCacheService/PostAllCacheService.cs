﻿using AutoMapper;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json;
using Pan.Common.Encryptions;
using Pan.Domain.Handler.Post;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Elasticsearch;
using Pan.Infrastructure.Entity;
using Pan.Infrastructure.EnumExceptions;
using Pan.Infrastructure.Enums;
using Pan.Infrastructure.Extensions;
using System;
using System.Linq;

namespace Pan.Domain.Caches.PostCacheService
{
    public class PostAllCacheService : CacheBase<PagedResultDto<PostItems>, string>
    {
        private readonly string Prefix = RedisCachePrefixEnums.Post.GetDescriptionByEnum();
        private readonly IRepository<post_thread> _threadRepository;
        public string Keyword = string.Empty;
        private readonly IMapper _mapper;
        public PostAllCacheService(IMemoryCache cache, IRepository<post_thread> threadRepository
            , IMapper mapper) : base(cache)
        {
            _mapper = mapper;
            _threadRepository = threadRepository;
            cacheName = Prefix;

            expiryTime = TimeSpan.FromHours(1);
            func = async () =>
            {
                Keyword = cacheKey.Replace(Prefix, "");
                var query = _threadRepository.GetQueryWithDisable()
                  .WhereIf(Keyword != "0", x => x.thread.Contains(Keyword))
                  .OrderByDescending(q => q.CreateOn)
                 ;
                var data = await query.OrderAndPagedAsync(new PagedAndSortedRequest());
                var items = data.Select(x =>
                {
                    var item = _mapper.Map<PostItems>(x);
                    item.IsCache = true;
                    item.PrimaryKey = x.HashV;
                    //item.HashV = x.HashV;
                    return item;
                });
                await RedisCache.GetOrDb(cacheName + Keyword, () =>
                {
                    return items;
                }, DateTime.Now.AddHours(1), 0);

                return new PagedResultDto<PostItems>(items, 0);
            };
        }

    }
}

