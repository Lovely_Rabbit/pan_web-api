﻿
//namespace Pan.Domain.Caches
//{
//public class RedisCacheBase
//{
//    //连接字符串
//    private string _connectionString;
//    //实例名称
//    private string _instanceName;
//    //默认数据库
//    private int _defaultDB = 0;

//    // private IDatabase CachRedis => GetConnect().GetDatabase(_defaultDB);

//    private ConcurrentDictionary<string, ConnectionMultiplexer> _connections;

//    public RedisCacheBase(string connectionString, string instanceName, int defaultDB = 0)
//    {
//        _connectionString = connectionString;
//        _instanceName = instanceName;
//        _defaultDB = defaultDB;
//        _connections = new ConcurrentDictionary<string, ConnectionMultiplexer>();
//    }

//    /// <summary>
//    /// 获取ConnectionMultiplexer
//    /// </summary>
//    /// <returns></returns> 
//    private ConnectionMultiplexer GetConnect()
//    {
//        Console.WriteLine($"Redis链接字符串：{_connectionString}，instanceName链接：{_instanceName}");
//        return _connections.GetOrAdd(_instanceName, p => ConnectionMultiplexer.Connect(_connectionString));
//    }


//    /// <summary>
//    /// 获取数据库
//    /// </summary>
//    /// <param name="configName"></param>
//    /// <param name="db">默认为0：优先代码的db配置，其次config中的配置</param>
//    /// <returns></returns>
//    public IDatabase GetDatabase(int? CaseDB)
//    {
//        if (CaseDB.HasValue)
//        {
//            _defaultDB = CaseDB.Value;
//        }

//        return GetConnect().GetDatabase(_defaultDB);
//    }

//    public IServer GetServer(string configName = null, int endPointsIndex = 0)
//    {
//        var confOption = ConfigurationOptions.Parse(_connectionString);
//        return GetConnect().GetServer(confOption.EndPoints[endPointsIndex]);

//    }

//    public ISubscriber GetSubscriber(string configName = null)
//    {
//        return GetConnect().GetSubscriber();
//    }

//    public void Dispose()
//    {
//        if (_connections != null && _connections.Count > 0)
//        {
//            foreach (var item in _connections.Values)
//            {
//                item.Close();
//            }
//        }
//    }

//        //    #region 
//        //    /// <summary>
//        //    /// 保存单个key value
//        //    /// </summary>
//        //    /// <param name="key">Redis Key</param>
//        //    /// <param name="value">保存的值</param>
//        //    /// <param name="expiry">过期时间</param>
//        //    /// <returns></returns>
//        //    public bool StringSet(string key, string value, TimeSpan? expiry = default(TimeSpan?))
//        //    {
//        //        return CachRedis.StringSet(key, value, expiry);
//        //    }
//        //    /// <summary>
//        //    /// 保存多个key value
//        //    /// </summary>
//        //    /// <param name="arr">key</param>
//        //    /// <returns></returns>
//        //    public bool StringSet(KeyValuePair<RedisKey, RedisValue>[] arr)
//        //    {
//        //        return CachRedis.StringSet(arr);
//        //    }
//        //    /// <summary>
//        //    /// 批量存值
//        //    /// </summary>
//        //    /// <param name="keysStr">key</param>
//        //    /// <param name="valuesStr">The value.</param>
//        //    /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
//        //    public bool StringSetMany(string[] keysStr, string[] valuesStr)
//        //    {
//        //        var count = keysStr.Length;
//        //        var keyValuePair = new KeyValuePair<RedisKey, RedisValue>[count];
//        //        for (int i = 0; i < count; i++)
//        //        {
//        //            keyValuePair[i] = new KeyValuePair<RedisKey, RedisValue>(keysStr[i], valuesStr[i]);
//        //        }

//        //        return CachRedis.StringSet(keyValuePair);
//        //    }

//        //    /// <summary>
//        //    /// 保存一个对象
//        //    /// </summary>
//        //    /// <typeparam name="T"></typeparam>
//        //    /// <param name="key"></param>
//        //    /// <param name="obj"></param>
//        //    /// <returns></returns>
//        //    public bool SetStringKey<T>(string key, T obj, TimeSpan? expiry = default(TimeSpan?))
//        //    {
//        //        string json = JsonConvert.SerializeObject(obj);
//        //        return CachRedis.StringSet(key, json, expiry);
//        //    }
//        //    /// <summary>
//        //    /// 追加值
//        //    /// </summary>
//        //    /// <param name="key"></param>
//        //    /// <param name="value"></param>
//        //    public void StringAppend(string key, string value)
//        //    {
//        //        ////追加值，返回追加后长度
//        //        long appendlong = CachRedis.StringAppend(key, value);
//        //    }

//        //    /// <summary>
//        //    /// 获取单个key的值
//        //    /// </summary>
//        //    /// <param name="key">Redis Key</param>
//        //    /// <returns></returns>
//        //    public RedisValue GetStringKey(string key)
//        //    {
//        //        return CachRedis.StringGet(key);
//        //    }
//        //    /// <summary>
//        //    /// 根据Key获取值
//        //    /// </summary>
//        //    /// <param name="key">键值</param>
//        //    /// <returns>System.String.</returns>
//        //    public string StringGet(string key)
//        //    {
//        //        try
//        //        {
//        //            return CachRedis.StringGet(key);
//        //        }
//        //        catch (Exception ex)
//        //        {
//        //            // Log.LogError("RedisHelper->StringGet 出错\r\n" + ex.ToString());
//        //            return null;
//        //        }
//        //    }

//        //    /// <summary>
//        //    /// 获取多个Key
//        //    /// </summary>
//        //    /// <param name="listKey">Redis Key集合</param>
//        //    /// <returns></returns>
//        //    public RedisValue[] GetStringKey(List<RedisKey> listKey)
//        //    {
//        //        return CachRedis.StringGet(listKey.ToArray());
//        //    }
//        //    /// <summary>
//        //    /// 批量获取值
//        //    /// </summary>
//        //    public string[] StringGetMany(string[] keyStrs)
//        //    {
//        //        var count = keyStrs.Length;
//        //        var keys = new RedisKey[count];
//        //        var addrs = new string[count];

//        //        for (var i = 0; i < count; i++)
//        //        {
//        //            keys[i] = keyStrs[i];
//        //        }
//        //        try
//        //        {

//        //            var values = CachRedis.StringGet(keys);
//        //            for (var i = 0; i < values.Length; i++)
//        //            {
//        //                addrs[i] = values[i];
//        //            }
//        //            return addrs;
//        //        }
//        //        catch (Exception ex)
//        //        {
//        //            // Log.LogError("RedisHelper->StringGetMany 出错\r\n" + ex.ToString());
//        //            return null;
//        //        }
//        //    }
//        //    /// <summary>
//        //    /// 获取一个key的对象
//        //    /// </summary>
//        //    /// <typeparam name="T"></typeparam>
//        //    /// <param name="key"></param>
//        //    /// <returns></returns>
//        //    public T GetStringKey<T>(string key)
//        //    {
//        //        return JsonConvert.DeserializeObject<T>(CachRedis.StringGet(key));
//        //    }

//        //    /// --删除设置过期--
//        //    /// <summary>
//        //    /// 删除单个key
//        //    /// </summary>
//        //    /// <param name="key">redis key</param>
//        //    /// <returns>是否删除成功</returns>
//        //    public bool KeyDelete(string key)
//        //    {
//        //        return CachRedis.KeyDelete(key);
//        //    }
//        //    /// <summary>
//        //    /// 删除多个key
//        //    /// </summary>
//        //    /// <param name="keys">rediskey</param>
//        //    /// <returns>成功删除的个数</returns>
//        //    public long KeyDelete(RedisKey[] keys)
//        //    {
//        //        return CachRedis.KeyDelete(keys);
//        //    }
//        //    /// <summary>
//        //    /// 判断key是否存储
//        //    /// </summary>
//        //    /// <param name="key">redis key</param>
//        //    /// <returns></returns>
//        //    public bool KeyExists(string key)
//        //    {
//        //        return CachRedis.KeyExists(key);
//        //    }
//        //    /// <summary>
//        //    /// 重新命名key
//        //    /// </summary>
//        //    /// <param name="key">就的redis key</param>
//        //    /// <param name="newKey">新的redis key</param>
//        //    /// <returns></returns>
//        //    public bool KeyRename(string key, string newKey)
//        //    {
//        //        return CachRedis.KeyRename(key, newKey);
//        //    }
//        //    /// <summary>
//        //    /// 删除hasekey
//        //    /// </summary>
//        //    /// <param name="key"></param>
//        //    /// <param name="hashField"></param>
//        //    /// <returns></returns>
//        //    public bool HaseDelete(RedisKey key, RedisValue hashField)
//        //    {
//        //        return CachRedis.HashDelete(key, hashField);
//        //    }
//        //    /// <summary>
//        //    /// 移除hash中的某值
//        //    /// </summary>
//        //    /// <typeparam name="T"></typeparam>
//        //    /// <param name="key"></param>
//        //    /// <param name="dataKey"></param>
//        //    /// <returns></returns>
//        //    public bool HashRemove(string key, string dataKey)
//        //    {
//        //        return CachRedis.HashDelete(key, dataKey);
//        //    }

//        //    /// <summary>
//        //    /// 设置缓存过期
//        //    /// </summary>
//        //    /// <param name="key"></param>
//        //    /// <param name="datetime"></param>
//        //    public void SetExpire(string key, DateTime datetime)
//        //    {
//        //        CachRedis.KeyExpire(key, datetime);
//        //    }

//        //}
//        //#endregion
//    }

//}