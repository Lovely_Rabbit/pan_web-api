﻿using AutoMapper;
using Pan.Domain.Handler.Tags;
using Pan.Domain.Handler.Test;
using Pan.Infrastructure.Entity;

namespace Pan.Domain.Mapper.Tags
{
    public class TagsMapper : Profile
    {
        public TagsMapper()
        {
            CreateMap<post, TagsGetAllResponse>();
            CreateMap<CachGetRequest, CachGetResponse>();
            CreateMap<post, CachGetResponse>();
            CreateMap<Tag, TagsTitleGetResponse>();
            CreateMap<Tag, TagsTitleGetDot>();
            CreateMap<TagsTitleGetDot, TagsTitleGetResponse>();
        }
    }
}
