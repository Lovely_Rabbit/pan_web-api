﻿using Pan.Infrastructure.Extensions;

namespace Pan.Domain.Model.Common
{
    public class PagedAndSortedRequest : IPagedAndSortedModel
    {

        public string Sorting { get; set; } = "CreateOn DESC";

        public virtual int SkipCount { get; set; } = 0;

        public virtual int MaxResultCount { get; set; } = 40;

        public bool SortingIsDefault()
        {
            return Sorting == "CreateOn DESC";
        }
    }

    public class PagedAndSortedIdRequest : IPagedAndSortedModel
    {

        public string Sorting { get; set; } = "Id DESC";

        public int SkipCount { get; set; } = 0;

        public virtual int MaxResultCount { get; set; } = 40;

        public bool SortingIsDefault()
        {
            return Sorting == "Id DESC";
        }
    }





    public class PagedRequest : IPagedModel
    {
        public int SkipCount { get; set; } = 0;

        public int MaxResultCount { get; set; } = 40;
    }
}
