﻿using Microsoft.AspNetCore.SignalR;
using Pan.Domain.Caches.RedisCacheService;
using System.Threading.Tasks;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using RabbitMQ.Client;

namespace pan.web.Hubs
{

    /// <summary>
    /// https://docs.microsoft.com/zh-cn/aspnet/core/signalr/introduction?view=aspnetcore-3.1
    /// https://docs.microsoft.com/zh-cn/aspnet/core/signalr/javascript-client?view=aspnetcore-6.0&tabs=visual-studio
    /// </summary>

    public class ChatHub : Hub
    {
        private readonly ICacheService _cacheService;


        //public static ConcurrentDictionary<string, string>  UserCache.ConnectionIds = new ConcurrentDictionary<string, string>();

        /// <summary>
        /// 构造 注入
        /// </summary>
        public ChatHub(ICacheService cacheService)
        {
            _cacheService = cacheService;
        }

        /// <summary>
        /// 建立连接时异步触发
        /// </summary>
        /// <returns></returns>
        public override async Task OnConnectedAsync()
        {
            //Console.WriteLine($"建立连接{Context.ConnectionId}");
            UserCache.Add(Context);
            //添加到一个组下
            //await Groups.AddToGroupAsync(Context.ConnectionId, "SignalR Users");
            //发送上线消息
            //await Clients.All.SendAsync("ReceiveHomePageMessage", 1, new { title = "系统消息", content = $"{Context.ConnectionId} 上线" });
            await base.OnConnectedAsync();
        }

        /// <summary>
        /// 离开连接时异步触发
        /// </summary>
        /// <param name="ex"></param>
        /// <returns></returns>
        public override async Task OnDisconnectedAsync(Exception ex)
        {
            //Console.WriteLine($"断开连接{Context.ConnectionId}");
            //从组中删除
            // await Groups.RemoveFromGroupAsync(Context.ConnectionId, "SignalR Users");
            //可自行调用下线业务处理方法...

            await UserOffline();
            //发送下线消息
            //   await Clients.All.SendAsync("ReceiveHomePageMessage", 4, new { title = "系统消息", content = $"{Context.ConnectionId} 离线" });
            await base.OnDisconnectedAsync(ex);
        }


        public async Task<bool> SendMessage(string msgv)
        {
            await Clients.All.SendAsync("ReceiveMessage", new { msg = "应答web：" + msgv, date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss") });
            return true;
        }


        /// <summary>
        /// 发送给指定的人
        /// </summary>
        /// <param name="username"></param>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<bool> SendHomeMessage(string username, string title, string message)
        {
            await Clients.Clients(UserCache.GetCnnectionIds(username)).SendAsync("ReceiveHomePageMessage", new
            {
                //   username,
                title,
                message,
                date = DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss")
            });
            return true;
        }

        /// <summary>
        /// 断开连接
        /// </summary>
        /// <returns></returns>
        public async Task<bool> UserOffline()
        {
            UserCache.Remove(Context);
            await Task.CompletedTask;
            return true;
        }
    }

    public static class UserCache
    {
        public static ConcurrentDictionary<string, string> ConnectionIds = new ConcurrentDictionary<string, string>();

        public static ConcurrentDictionary<string, int> Online = new ConcurrentDictionary<string, int>();

        /// <summary>
        /// 根据用户名获取所有的客户端
        /// </summary>
        /// <param name="username"></param>
        /// <returns></returns>
        public static IEnumerable<string> GetCnnectionIds(string username)
        {
            foreach (var item in ConnectionIds)
            {
                if (item.Value == username)
                {
                    yield return item.Key;
                }
            }
        }

        public static int GetOnline(string username)
        {

            if (Online.TryGetValue(username, out int val))
            {
                return val;
            }
            return 0;
        }



        public static void Add(HubCallerContext context)
        {
            string userName = context.GetHttpContext().Request.Query["userName"].ToString();
            if (string.IsNullOrEmpty(userName))
            {
                return;
            }
            Online[userName] = 1;
            ConnectionIds[context.ConnectionId] = userName;
        }

        public static void Remove(HubCallerContext context)
        {
            var cid = context.ConnectionId;

            //移除缓存
            if (ConnectionIds.TryRemove(cid, out string value))
            {
                Online[value] = 0;
            }
        }
    }

}
