﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using pan.web.BaseControllers;
using Pan.Domain.Handler.Post;
using Pan.Domain.Model.Common;
using System.Net.Http.Headers;
using System.Net.Http;
using System;
using System.Threading.Tasks;
using System.Net;
using Pan.Infrastructure.MongdbContext;
using Pan.Infrastructure.Entity;

namespace pan.web.Controllers
{
    /// <summary>
    /// 文章
    /// </summary>
    [AllowAnonymous]
    public class PostController : BaseController
    {

        public PostController(IMediator mediator, IMemoryCache cache
            ) : base(mediator, cache)
        {
        }

        /// <summary>
        /// 获取所有 资源
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<PagedResultDto<PostItems>>))]
        public async Task<IActionResult> GetAll([FromQuery] PostRequest request)
        {
            var result = await _mediator.Send(request);
            return Json(new ResponseValue<PagedResultDto<PostItems>>(result));
        }

        /// <summary>
        /// 获取单个文章
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<PostsGetResponse>))]
        public async Task<IActionResult> Get([FromQuery] PostsGetRequest request)
        {
            PostsGetResponse result = await _mediator.Send(request);
            return Json(new ResponseValue<PostsGetResponse>(result));
        }

        /// <summary>
        ///  获取key是否有效
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<HttpStatusCode>))]
        public async Task<IActionResult> GetShare([FromQuery] ShareIdRequest request)
        {
            HttpStatusCode result = await _mediator.Send(request);
            return Json(new ResponseValue<HttpStatusCode>(result));
        }

        /// <summary>
        /// 获取分词
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<PagedResultDto<PostItems>>))]
        public async Task<IActionResult> GetToken([FromQuery] GetTokenRequest request)
        {
            var result = await _mediator.Send(request);
            return Json(new ResponseValue<PagedResultDto<PostItems>>(result));
        }
    }
}
