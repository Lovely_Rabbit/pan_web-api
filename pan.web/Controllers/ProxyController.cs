﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using pan.web.BaseControllers;
using Pan.Domain.Handler.Proxy;
using Pan.Domain.Model.Common;
using System.Threading.Tasks;

namespace pan.web.Controllers
{
    public class ProxyController : BaseController
    {
        public ProxyController(IMediator mediator, IMemoryCache cache) : base(mediator, cache)
        {
        }


        [HttpGet]
        [AllowAnonymous]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<ProxyResponse>))]
        public async Task<IActionResult> Get([FromQuery] ProxyRequest request)
        {
            var result = await _mediator.Send(request);
            return Json(new ResponseValue<ProxyResponse>(result));
        }
    }



}
