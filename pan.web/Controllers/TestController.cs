﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using pan.web.BaseControllers;
using Pan.Domain.Handler.Test;
using Pan.Domain.Model.Common;
using System.Threading.Tasks;

namespace pan.web.Controllers
{
    /// <summary>
    /// 测试缓存
    /// </summary>
    [AllowAnonymous]
    public class TestController : BaseController
    {
        //  public IMyPublisher<CachGetResponse> _testPublisher;

        public TestController(IMediator mediator, IMemoryCache cache) : base(mediator, cache)
        {

        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<PagedResultDto<CachGetResponse>>))]
        public async Task<IActionResult> Get([FromQuery] CachGetRequest request)
        {
            //var data = new CachGetResponse
            //{

            //};
            //await _testPublisher.PublishAsync(data);

            var result = await _mediator.Send(request);
            return Json(new ResponseValue<CachGetResponse>(result));
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue))]
        public async Task<IActionResult> GetAll([FromQuery] SubscribeGetAllRequest request)
        {
            var result = await _mediator.Send(request);
            if (result) return Json(new ResponseValue());
            else return Json(new ResponseValue("操作失败！"));
        }

        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue))]
        public async Task<IActionResult> TestEmail([FromQuery] TestEmailRequest request)
        {
            var result = await _mediator.Send(request);
            if (result) return Json(new ResponseValue());
            else return Json(new ResponseValue("操作失败！"));
        }
    }
}
