﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using pan.web.BaseControllers;
using Pan.Domain.Handler.HotNews;
using Pan.Domain.Model.Common;
using System.Threading.Tasks;

namespace pan.web.Controllers
{
    public class HotNewController : BaseController
    {
        public HotNewController(IMediator mediator, IMemoryCache cache) : base(mediator, cache)
        {

        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        //[ResponseCache]
        [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ResponseValue<PagedResultDto<HotItems>>))]
        public async Task<IActionResult> GetAll([FromQuery] HotRequest request)
        {
            var result = await _mediator.Send(request);
            return Json(new ResponseValue<PagedResultDto<HotItems>>(result));
        }


    }
}
