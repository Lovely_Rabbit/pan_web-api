﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Pan.Infrastructure.MongdbContext;
using System;
namespace pan.web.Extensions
{
    public static class MongodbContextServiceCollectionExtensions
    {

        public static IServiceCollection AddMongoDBContext<T>(this IServiceCollection services
            , Action<MongoDBContextOptions> setupAction) where T : MongoDBContext
        {

            if (services == null)
            { throw new ArgumentNullException(nameof(services)); }
            if (setupAction == null) { throw new ArgumentNullException(nameof(setupAction)); }
            services.Configure(setupAction);
            //services.AddSingleton<T>();
            services.AddScoped<IMongoContext, T>();
           
            return services;
        }
    }
}
