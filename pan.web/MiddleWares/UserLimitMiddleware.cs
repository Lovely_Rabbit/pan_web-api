﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.Context;
using Pan.Infrastructure.Entity;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace pan.web.MiddleWares
{
    public class UserLimitMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IMemoryCache memCache;

        public UserLimitMiddleware(IMemoryCache memCache)
        {
            this.memCache = memCache;
        }

        public UserLimitMiddleware(RequestDelegate next, IMemoryCache memCache)
        {
            _next = next;
            this.memCache = memCache;
        }

        public async Task Invoke(HttpContext context, ILogger<UserLimitMiddleware> logger, EFCoreDbContext dbContext)
        {
            // 切割请求地址
            var type = context.Request.Path.ToString().Split('.').Last();
            var ips = context.Request.Headers["HTTP_VIA"].ToString();
            string queryParam = context.Request.QueryString.ToString().Split('=').Last();
            // string name = HttpUtility.UrlEncode(queryParam, Encoding.UTF8);
            queryParam = HttpUtility.UrlDecode(queryParam, Encoding.UTF8);
            logger.LogInformation(context.Request.Path + "\t" + context.Connection.RemoteIpAddress.MapToIPv4().ToString() + "\t" + context.Request.Headers["X-Real-IP"] + "\t" + context.Request.Headers["X-Forwarded-For"] + "\t" + context.Request.Headers["Referer"]);
            //if (context.Connection.RemoteIpAddress.MapToIPv4().ToString() != "0.0.0.1")=======
            //{
            var log = new AccessLog()
            {
                CreateOn = DateTime.Now,
                request_date = DateTime.Now,
                request_ip = string.IsNullOrEmpty(context.Request.Headers["X-Real-IP"].ToString()) ? context.Connection.RemoteIpAddress.MapToIPv4().ToString() : context.Request.Headers["X-Real-IP"].ToString(),
                request_ForwardIp = string.IsNullOrEmpty(context.Request.Headers["X-Forwarded-For"].ToString()) ? context.Connection.RemoteIpAddress.MapToIPv4().ToString() : context.Request.Headers["X-Forwarded-For"].ToString(),
                Referer = context.Request.Headers["Referer"].ToString(),
                request_url = context.Request.Path.ToString(),
                User_Agent = context.Request.Headers["User-Agent"].ToString(),
                Params = queryParam.ToString(),
                request_count = 1
            };
            try
            {
                var any = await dbContext.Set<AccessLog>().AnyAsync(x => x.request_ForwardIp == log.request_ForwardIp && x.User_Agent == log.User_Agent && x.request_ip == log.request_ip && x.request_url == log.request_url);
                if (any)
                {
                    var query = await dbContext.Set<AccessLog>().FirstOrDefaultAsync(x => x.request_ForwardIp == log.request_ForwardIp && x.User_Agent == log.User_Agent && x.request_ip == log.request_ip && x.request_url == log.request_url);
                    query.request_count = query.request_count + 1;
                    query.Params = queryParam.ToString();
                    dbContext.Update(query);
                }
                else
                {
                    await dbContext.AddAsync(log);
                }
                //var paramq = await dbContext.Set<queryparam>().FirstOrDefaultAsync(x => x.Query_param == queryParam);

                var paramqAny = await dbContext.Set<queryparam>().AnyAsync(x => x.Query_param == queryParam);
                if (paramqAny)
                {
                    await dbContext.AddAsync(paramqAny);
                }
                else
                {
                    var item = await dbContext.Set<queryparam>().FirstOrDefaultAsync(x => x.Query_param == queryParam);
                    var queryparam = new queryparam()
                    {
                        Query_param = queryParam,
                        Query_count = 1

                    };
                    item.Query_count = item.Query_count + 1;
                    dbContext.Update(item);
                }
                await dbContext.SaveChangesAsync();
            }
            catch (Exception)
            {
                var data = JsonConvert.SerializeObject(new ResponseValue()
                {
                    Code = 400,
                    ErrorMsg = "请求失败！请重新操作"
                });
                await context.Response.WriteAsync(data); //短路
            }


            //}
            await _next(context);
        }
    }
    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class UserLimitMiddlewareExtensions
    {
        public static IApplicationBuilder UserLimitMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<UserLimitMiddleware>();
        }
    }
}
