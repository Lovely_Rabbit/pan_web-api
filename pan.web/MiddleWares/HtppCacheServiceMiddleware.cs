﻿using Amazon.Runtime.Internal.Util;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Nest;
using Newtonsoft.Json;
using Pan.Domain.Caches;
using Pan.Domain.Handler.Post;
using Pan.Domain.Model.Common;
using Pan.Infrastructure.EnumExceptions;
using Pan.Infrastructure.Enums;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace pan.web.MiddleWares
{
    public class HtppCacheServiceMiddleware : TaskVerifyDic
    {
        private readonly string Prefix = RedisCachePrefixEnums.Post.GetDescriptionByEnum();
        //命中所以http get请求

        private readonly RequestDelegate _next;
        protected readonly IMemoryCache _cache;
        private readonly ILogger<HtppCacheServiceMiddleware> _logger;

        public HtppCacheServiceMiddleware(RequestDelegate next, IMemoryCache cache, ILogger<HtppCacheServiceMiddleware> logger)
        {
            _cache = cache;
            _logger = logger;
            _next = next;
        }
        public async Task Invoke(HttpContext httpContext)
        {
            //app.MapWhen(context => context.Request.Query.ContainsKey("/api/Post/GetAll"),
            //http://xh.bozaiq.cn:8084/api/Post/GetAll?&Keyword=11

            var actionKey = httpContext.Request.RouteValues;


            var acitonName = actionKey.ContainsKey("action") ? actionKey["action"].ToString().ToLower() : "";
            var paramter = httpContext.Request.QueryString.Value == "?Keyword=";
            try
            {
                if (!paramter)
                {
                    await _next(httpContext);
                }
                //Console.WriteLine("acitonName" + acitonName);
                else if (acitonName == "getall")
                {
                    //_cache.Remove(Prefix + "0");
                    if (_cache.Get(Prefix + "0") != null)
                    {
                        httpContext.Response.StatusCode = 200;
                        httpContext.Response.ContentType = "application/json; charset=utf-8";
                        var items = _cache.Get<PagedResultDto<PostItems>>(Prefix + "0");
                        ResponseValue<PagedResultDto<PostItems>> data = new ResponseValue<PagedResultDto<PostItems>>(items);
                        _logger.LogInformation("内存缓存==>");
                        await httpContext.Response.WriteAsJsonAsync(data);
                        return;

                    }
                    //Htpp中间件处理缓存  //TODO 二级缓存 （nginx + lua 转发goland 缓存 local）
                    else if (await RedisCache.ExistsAsync(Prefix + "0", 0))
                    {
                        httpContext.Response.StatusCode = 200;
                        httpContext.Response.ContentType = "application/json; charset=utf-8";
                        var items = JsonConvert.DeserializeObject<PagedResultDto<PostItems>>
                            (await RedisCache.GetAsync(Prefix + "0", 0));
                        var data = new ResponseValue<PagedResultDto<PostItems>>(items);
                        _logger.LogInformation("Redis缓存==>");
                        await httpContext.Response.WriteAsJsonAsync(data);
                        return;
                    }
                    else
                    {
                        await _next(httpContext);
                    }
                }
                else
                {
                    await _next(httpContext);
                }

            }
            catch (System.Exception ex)
            {
                Console.Write("exerror:=>" + ex);
            }

        }
    }

    public static class HtppCacheServiceMiddlewareExtensions
    {
        public static IApplicationBuilder HtppCacheServiceMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<HtppCacheServiceMiddleware>();
        }
    }
}
