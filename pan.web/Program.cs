using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.IO;

namespace pan.web
{
    public class Program
    {
        public static void Main(string[] args)
        {

            //InitTimer();
            //setTaskAtFixedTime();
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Information()
                //.MinimumLevel.Override("System", Serilog.Events.LogEventLevel.Warning)
                .MinimumLevel.Override("Microsoft", Serilog.Events.LogEventLevel.Information)
                //.MinimumLevel.Override("Microsoft.AspNetCore.Mvc", Serilog.Events.LogEventLevel.Information)
                //.ReadFrom.Configuration(Configuration) // �������ļ���ȡ
                .Enrich.FromLogContext()
                .WriteTo.Console()
                .WriteTo.File(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "logs", ".log"),
                rollingInterval: RollingInterval.Day)
                .CreateLogger();
            Log.Information("application start");

            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "host terminated unexpectedly");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {
            var hostBuilder = Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(builder =>
                {
                    builder.UseUrls("http://*:81");
                    builder.ConfigureKestrel(opt =>
                    {
                        //var cert = new X509Certificate2("cert/panwebApi.pfx", "123654");
                        //opt.ConfigureHttpsDefaults(x =>
                        //{
                        //    x.ClientCertificateMode = Microsoft.AspNetCore.Server.Kestrel.Https.ClientCertificateMode.AllowCertificate;
                        //    x.CheckCertificateRevocation = false;
                        //    x.ServerCertificate = cert;
                        //});

                    });
                    builder.UseStartup<Startup>();
                })
                .UseSerilog();

            return hostBuilder;
        }
    }
}
